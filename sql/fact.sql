WITH GENERATE_MONTH AS (
  -- SELECT
  --   DISTINCT 
  --   DATE_TRUNC(DATE(TRANSACTION_DATETIME),MONTH) AS TARGET_MONTH
  --   ,MOBILE
  --   ,MAX(IF(VAS_PACKAGE_CODE LIKE '%_RC_%', 1, 0)) AS RC_FLAG
  -- FROM `acm-prod.datamart.DM_VAS_TOPUP_ALL_CHANNEL_TRANSACTION`
  -- WHERE DATE_TRUNC(DATE(TRANSACTION_DATETIME),MONTH) BETWEEN '2020-06-01' AND '2021-02-01'
  -- GROUP BY 1,2
  SELECT TARGET_MONTH
  FROM (
    SELECT GENERATE_DATE_ARRAY(DATE('2020-06-01'), DATE('2021-02-01'), INTERVAL 1 MONTH) AS MONTH_ARRAY
  ), UNNEST(MONTH_ARRAY) AS TARGET_MONTH
),
CH_SOF AS (
  SELECT
    DISTINCT 
      DATE_TRUNC(DATE(TRANS_DATETIME),MONTH) AS MONTH
      ,MOBILE
      --- SOF
      ,SUM(SOF_WALLET) AS SOF_WALLET
      ,SUM(SOF_AIRTIME) AS SOF_AIRTIME

      --- BIZ CH
      ,SUM(CH_WALLET) AS CH_WALLET
      ,SUM(CH_WEB) AS CH_WEB
  FROM (
    SELECT 
      TRANS_DATETIME
      ,COALESCE(MOBILE_RECEIVER,MOBILE_BUYER) AS MOBILE
      ,SOURCEOFFUND
      ,IF(SOURCEOFFUND = 'E-WALLET', 1, 0) AS SOF_WALLET
      ,IF(SOURCEOFFUND = 'AIRTIME', 1, 0) AS SOF_AIRTIME
      ,IF(BIZ_CHANNEL = 'TMN WEB', 1, 0) AS CH_WEB
      ,IF(BIZ_CHANNEL = 'TMN WALLET', 1, 0) AS CH_WALLET
    FROM `acm-prod.datamart.DM_VAS_TOPUP_TRANSACTION`
    WHERE 1=1
      AND DATE_TRUNC(DATE(TRANS_DATETIME),MONTH) BETWEEN '2019-12-01' AND '2021-02-01'
      AND TRMV_TYPE = 'PRE'
      AND MERCHANT_ID NOT LIKE '%68%'
  )
  GROUP BY 1,2
),
TARGET_RC AS (
  SELECT DISTINCT DATE_TRUNC(DATE(TRANSACTION_DATETIME),MONTH) AS MONTH, MOBILE
  FROM `acm-prod.datamart.DM_VAS_TOPUP_ALL_CHANNEL_TRANSACTION`
  WHERE VAS_PACKAGE_CODE LIKE '%_RC_%'
),
RECENCY_RC AS (
  SELECT 
    MOBILE
    ,TARGET_MONTH
    ,MIN(DATE_DIFF(TARGET_MONTH,MONTH,MONTH)) AS RC_RECENCY
  FROM (
    SELECT DISTINCT DATE_TRUNC(DATE(TRANSACTION_DATETIME),MONTH) AS MONTH, MOBILE
    FROM `acm-prod.datamart.DM_VAS_TOPUP_ALL_CHANNEL_TRANSACTION`
    WHERE VAS_PACKAGE_CODE LIKE '%_RC_%'
  ), GENERATE_MONTH
  WHERE TARGET_MONTH > MONTH
  GROUP BY 1,2
),
FIRST_RC AS (
  SELECT MOBILE,MIN(DATE_TRUNC(DATE(TRANSACTION_DATETIME),MONTH)) AS FIRST_RC_MONTH
  FROM `acm-prod.datamart.DM_VAS_TOPUP_ALL_CHANNEL_TRANSACTION`
  WHERE VAS_PACKAGE_CODE LIKE '%_RC_%'
  GROUP BY 1
),
VARIETY_6M AS (
  SELECT 
    MOBILE
    ,TARGET_MONTH
    ,COUNT(DISTINCT VAS_PACKAGE_CODE) AS V_CODE_6M
    ,COUNT(DISTINCT VAS_PACKAGE_TYPE) AS V_TYPE_6M
    ,COUNT(DISTINCT VAS_PACKAGE_VALIDITY) AS V_VALIDITY_6M
  FROM `acm-prod.datamart.DM_VAS_TOPUP_ALL_CHANNEL_TRANSACTION`, GENERATE_MONTH
  WHERE DATE_TRUNC(DATE(TRANSACTION_DATETIME),MONTH) BETWEEN DATE_SUB(GENERATE_MONTH.TARGET_MONTH, INTERVAL 6 MONTH) AND DATE_SUB(GENERATE_MONTH.TARGET_MONTH, INTERVAL 1 MONTH)
  AND TRMV_TYPE = 'PRE'
  GROUP BY 1,2
),
MAIN AS (
  SELECT
    MOBILE
    ,TARGET_MONTH
    ,MIN(MONTH) AS FIRST_MONTH
    ,MAX(MONTH) AS LAST_MONTH
    ,COUNT(1) AS TOTAL_MONTH
    ,SUM(TPV) AS TPV_6M
    ,SUM(TPV)/COUNT(1) AS TPV_1M
    ,SUM(TPV)/SUM(FREQ) AS TS
    ,SUM(FREQ) AS FREQ_6M
    ,AVG(FREQ) AS FREQ_1M
    ,SUM(COVER_DAY) AS COVER_DAY_6M
    ,AVG(COVER_DAY) AS COVER_DAY_1M

    --- PACKAGE TYPE 6M
    ,SUM(PT_VOICE_FREQ) AS PT_VOICE_FREQ_6M
    ,SUM(PT_FIX_SPEED_NS_FREQ) AS PT_FIX_SPEED_NS_FREQ_6M
    ,SUM(PT_FIX_SPEED_UL_FREQ) AS PT_FIX_SPEED_UL_FREQ_6M
    ,SUM(PT_FULL_SPEED_NS_FREQ) AS PT_FULL_SPEED_NS_FREQ_6M
    ,SUM(PT_BURST_SPEED_FREQ) AS PT_BURST_SPEED_FREQ_6M
    ,SUM(PT_BUNDLE_APP_FREQ) AS PT_BUNDLE_APP_FREQ_6M
    ,SUM(PT_OTHER_FREQ) AS PT_OTHER_FREQ_6M

    --- PACKAGE TYPE 1M
    ,AVG(PT_VOICE_FREQ) AS PT_VOICE_FREQ_1M
    ,AVG(PT_FIX_SPEED_NS_FREQ) AS PT_FIX_SPEED_NS_FREQ_1M
    ,AVG(PT_FIX_SPEED_UL_FREQ) AS PT_FIX_SPEED_UL_FREQ_1M
    ,AVG(PT_FULL_SPEED_NS_FREQ) AS PT_FULL_SPEED_NS_FREQ_1M
    ,AVG(PT_BURST_SPEED_FREQ) AS PT_BURST_SPEED_FREQ_1M
    ,AVG(PT_BUNDLE_APP_FREQ) AS PT_BUNDLE_APP_FREQ_1M
    ,AVG(PT_OTHER_FREQ) AS PT_OTHER_FREQ_1M

    --- PACKAGE TYPE COVER 6M
    ,SUM(PT_VOICE_COVER) AS PT_VOICE_COVER_6M
    ,SUM(PT_FIX_SPEED_NS_COVER) AS PT_FIX_SPEED_NS_COVER_6M
    ,SUM(PT_FIX_SPEED_UL_COVER) AS PT_FIX_SPEED_UL_COVER_6M
    ,SUM(PT_FULL_SPEED_NS_COVER) AS PT_FULL_SPEED_NS_COVER_6M
    ,SUM(PT_BURST_SPEED_COVER) AS PT_BURST_SPEED_COVER_6M
    ,SUM(PT_BUNDLE_APP_COVER) AS PT_BUNDLE_APP_COVER_6M
    ,SUM(PT_OTHER_COVER) AS PT_OTHER_COVER_6M

    --- PACKAGE TYPE COVER 1M
    ,AVG(PT_VOICE_COVER) AS PT_VOICE_COVER_1M
    ,AVG(PT_FIX_SPEED_NS_COVER) AS PT_FIX_SPEED_NS_COVER_1M
    ,AVG(PT_FIX_SPEED_UL_COVER) AS PT_FIX_SPEED_UL_COVER_1M
    ,AVG(PT_FULL_SPEED_NS_COVER) AS PT_FULL_SPEED_NS_COVER_1M
    ,AVG(PT_BURST_SPEED_COVER) AS PT_BURST_SPEED_COVER_1M
    ,AVG(PT_BUNDLE_APP_COVER) AS PT_BUNDLE_APP_COVER_1M
    ,AVG(PT_OTHER_COVER) AS PT_OTHER_COVER_1M

    --- PACKAGE VALIDITY 6M
    ,SUM(V_1D_FREQ) AS V_1D_FREQ_6M
    ,SUM(V_2D_FREQ) AS V_2D_FREQ_6M
    ,SUM(V_3D_FREQ) AS V_3D_FREQ_6M
    ,SUM(V_5D_FREQ) AS V_5D_FREQ_6M
    ,SUM(V_7D_FREQ) AS V_7D_FREQ_6M
    ,SUM(V_8D_FREQ) AS V_8D_FREQ_6M
    ,SUM(V_10D_FREQ) AS V_10D_FREQ_6M
    ,SUM(V_15D_FREQ) AS V_15D_FREQ_6M
    ,SUM(V_30D_FREQ) AS V_30D_FREQ_6M
    ,SUM(V_90D_FREQ) AS V_90D_FREQ_6M
    ,SUM(V_180D_FREQ) AS V_180D_FREQ_6M
    ,SUM(V_OTHER_FREQ) AS V_OTHER_FREQ_6M

    --- PACKAGE VALIDITY 1M
    ,AVG(V_1D_FREQ) AS V_1D_FREQ_1M
    ,AVG(V_2D_FREQ) AS V_2D_FREQ_1M
    ,AVG(V_3D_FREQ) AS V_3D_FREQ_1M
    ,AVG(V_5D_FREQ) AS V_5D_FREQ_1M
    ,AVG(V_7D_FREQ) AS V_7D_FREQ_1M
    ,AVG(V_8D_FREQ) AS V_8D_FREQ_1M
    ,AVG(V_10D_FREQ) AS V_10D_FREQ_1M
    ,AVG(V_15D_FREQ) AS V_15D_FREQ_1M
    ,AVG(V_30D_FREQ) AS V_30D_FREQ_1M
    ,AVG(V_90D_FREQ) AS V_90D_FREQ_1M
    ,AVG(V_180D_FREQ) AS V_180D_FREQ_1M
    ,AVG(V_OTHER_FREQ) AS V_OTHER_FREQ_1M

    --- SOF 6M
    ,SUM(SOF_WALLET_FREQ) AS SOF_WALLET_FREQ_6M
    ,SUM(SOF_AIRTIME_FREQ) AS SOF_AIRTIME_FREQ_6M
    ,SUM(SOF_OTHER_FREQ) AS SOF_OTHER_FREQ_6M
    --- SOF 1M
    ,AVG(SOF_WALLET_FREQ) AS SOF_WALLET_FREQ_1M
    ,AVG(SOF_AIRTIME_FREQ) AS SOF_AIRTIME_FREQ_1M
    ,AVG(SOF_OTHER_FREQ) AS SOF_OTHER_FREQ_1M

    --- CH 6M
    ,SUM(CH_WALLET_FREQ) AS CH_WALLET_FREQ_6M
    ,SUM(CH_WEB_FREQ) AS CH_WEB_FREQ_6M
    ,SUM(CH_OTHER_FREQ) AS CH_OTHER_FREQ_6M
    --- CH 1M
    ,AVG(CH_WALLET_FREQ) AS CH_WALLET_FREQ_1M
    ,AVG(CH_WEB_FREQ) AS CH_WEB_FREQ_1M
    ,AVG(CH_OTHER_FREQ) AS CH_OTHER_FREQ_1M

    -- ,MAX(IF(TARGET_MONTH > RC.MONTH, DATE_DIFF(TARGET_MONTH, RC.MONTH, MONTH), -1)) AS RC_RECENCY

    --- VARIETY PACKAGE
    ,AVG(V_CODE) AS V_CODE_1M
    ,AVG(V_TYPE) AS V_TYPE_1M
    ,AVG(V_VALIDITY) AS V_VALIDITY_1M

    -- ,MAX(RC_FLAG) AS RC_FLAG
  FROM (
    SELECT 
      A.*
      ,TARGET_MONTH
      --- SOF
      ,COALESCE(SOF_WALLET,0) AS SOF_WALLET_FREQ
      ,COALESCE(SOF_AIRTIME,0) AS SOF_AIRTIME_FREQ
      ,IF(FREQ-COALESCE(SOF_WALLET,0)-COALESCE(SOF_AIRTIME,0) < 0, 0, FREQ-COALESCE(SOF_WALLET,0)-COALESCE(SOF_AIRTIME,0)) AS SOF_OTHER_FREQ
      --- CH
      ,COALESCE(CH_WALLET,0) AS CH_WALLET_FREQ
      ,COALESCE(CH_WEB,0) AS CH_WEB_FREQ
      ,IF(FREQ-COALESCE(CH_WALLET,0)-COALESCE(CH_WEB,0) < 0, 0, FREQ-COALESCE(CH_WALLET,0)-COALESCE(CH_WEB,0)) AS CH_OTHER_FREQ

    FROM (
      SELECT
        DATE_TRUNC(DATE(TRANSACTION_DATETIME),MONTH) AS MONTH
        ,ALLCH.MOBILE
        ,SUM(VAS_PACKAGE_AMOUNT) AS TPV
        ,AVG(VAS_PACKAGE_AMOUNT) AS TS
        ,COUNT(1) AS FREQ
        ,SUM(VALIDITY) AS COVER_DAY

        -- VARIETY
        ,COUNT(DISTINCT VAS_PACKAGE_CODE) AS V_CODE
        ,COUNT(DISTINCT VAS_PACKAGE_TYPE) AS V_TYPE
        ,COUNT(DISTINCT VAS_PACKAGE_VALIDITY) AS V_VALIDITY
        --- PACKAGE TYPE
        ,SUM(PT_VOICE) AS PT_VOICE_FREQ
        ,SUM(PT_FIX_SPEED_NS) AS PT_FIX_SPEED_NS_FREQ
        ,SUM(PT_FIX_SPEED_UL) AS PT_FIX_SPEED_UL_FREQ
        ,SUM(PT_FULL_SPEED_NS) AS PT_FULL_SPEED_NS_FREQ
        ,SUM(PT_BURST_SPEED) AS PT_BURST_SPEED_FREQ
        ,SUM(PT_BUNDLE_APP) AS PT_BUNDLE_APP_FREQ
        ,SUM(PT_OTHER) AS PT_OTHER_FREQ

        ,SUM(IF(PT_VOICE = 1, VALIDITY, 0)) AS PT_VOICE_COVER
        ,SUM(IF(PT_FIX_SPEED_NS = 1, VALIDITY, 0)) AS PT_FIX_SPEED_NS_COVER
        ,SUM(IF(PT_FIX_SPEED_UL = 1, VALIDITY, 0)) AS PT_FIX_SPEED_UL_COVER
        ,SUM(IF(PT_FULL_SPEED_NS = 1, VALIDITY, 0)) AS PT_FULL_SPEED_NS_COVER
        ,SUM(IF(PT_BURST_SPEED = 1, VALIDITY, 0)) AS PT_BURST_SPEED_COVER
        ,SUM(IF(PT_BUNDLE_APP = 1, VALIDITY, 0)) AS PT_BUNDLE_APP_COVER
        ,SUM(IF(PT_OTHER = 1, VALIDITY, 0)) AS PT_OTHER_COVER

        --- PACKAGE VALIDITY
        ,SUM(V_1D) AS V_1D_FREQ
        ,SUM(V_2D) AS V_2D_FREQ
        ,SUM(V_3D) AS V_3D_FREQ
        ,SUM(V_5D) AS V_5D_FREQ
        ,SUM(V_7D) AS V_7D_FREQ
        ,SUM(V_8D) AS V_8D_FREQ
        ,SUM(V_10D) AS V_10D_FREQ
        ,SUM(V_15D) AS V_15D_FREQ
        ,SUM(V_30D) AS V_30D_FREQ
        ,SUM(V_90D) AS V_90D_FREQ
        ,SUM(V_180D) AS V_180D_FREQ
        ,SUM(V_OTHER) AS V_OTHER_FREQ

      FROM (
        SELECT
          TRANSACTION_DATETIME
          ,MOBILE
          ,VAS_PACKAGE_AMOUNT
          ,VAS_PACKAGE_TYPE
          ,VAS_PACKAGE_CODE
          ,VAS_PACKAGE_VALIDITY
          ,CASE
            WHEN VAS_PACKAGE_VALIDITY IN ('1D','24H') THEN 1
            WHEN VAS_PACKAGE_VALIDITY IN ('2D','48H') THEN 2
            WHEN VAS_PACKAGE_VALIDITY IN ('3D','72H') THEN 3
            WHEN VAS_PACKAGE_VALIDITY IS NULL THEN -1
            ELSE CAST(REGEXP_EXTRACT(VAS_PACKAGE_VALIDITY , '^[0-9]+') AS INT64)
          END AS VALIDITY
          --- PACKAGE TYPE
          ,IF(VAS_PACKAGE_TYPE = 'FULL_SPEED_NS', 1, 0) AS PT_FULL_SPEED_NS
          ,IF(VAS_PACKAGE_TYPE = 'VOICE', 1, 0) AS PT_VOICE
          ,IF(VAS_PACKAGE_TYPE = 'FIX_SPEED_NS', 1, 0) AS PT_FIX_SPEED_NS
          ,IF(VAS_PACKAGE_TYPE = 'FIX_SPEED_UL', 1, 0) AS PT_FIX_SPEED_UL
          ,IF(VAS_PACKAGE_TYPE = 'BURST_SPEED', 1, 0) AS PT_BURST_SPEED
          ,IF(VAS_PACKAGE_TYPE = 'BUNDLE_APP', 1, 0) AS PT_BUNDLE_APP
          ,IF(VAS_PACKAGE_TYPE = 'OTHER', 1, 0) AS PT_OTHER

          --- PACKAGE VALIDITY
          ,IF(VAS_PACKAGE_VALIDITY IN ('1D','24H'), 1, 0) AS V_1D
          ,IF(VAS_PACKAGE_VALIDITY IN ('2D','48H'), 1, 0) AS V_2D
          ,IF(VAS_PACKAGE_VALIDITY IN ('3D','72H'), 1, 0) AS V_3D
          ,IF(VAS_PACKAGE_VALIDITY IN ('5D'), 1, 0) AS V_5D
          ,IF(VAS_PACKAGE_VALIDITY IN ('7D'), 1, 0) AS V_7D
          ,IF(VAS_PACKAGE_VALIDITY IN ('8D'), 1, 0) AS V_8D
          ,IF(VAS_PACKAGE_VALIDITY IN ('10D'), 1, 0) AS V_10D
          ,IF(VAS_PACKAGE_VALIDITY IN ('15D'), 1, 0) AS V_15D
          ,IF(VAS_PACKAGE_VALIDITY IN ('30D'), 1, 0) AS V_30D
          ,IF(VAS_PACKAGE_VALIDITY IN ('90D'), 1, 0) AS V_90D
          ,IF(VAS_PACKAGE_VALIDITY IN ('180D'), 1, 0) AS V_180D
          ,IF(VAS_PACKAGE_VALIDITY IS NULL OR VAS_PACKAGE_VALIDITY IN ('6D','240D','365D'), 1, 0) AS V_OTHER

        FROM `acm-prod.datamart.DM_VAS_TOPUP_ALL_CHANNEL_TRANSACTION`
        WHERE 1=1
          AND DATE_TRUNC(DATE(TRANSACTION_DATETIME),MONTH) BETWEEN '2019-12-01' AND '2021-02-01'
          AND TRMV_TYPE = 'PRE'
      ) ALLCH
      WHERE 1=1 
      AND VALIDITY <> -1
      AND VAS_PACKAGE_TYPE IS NOT NULL
      GROUP BY 1,2
    ) A,GENERATE_MONTH
    LEFT JOIN CH_SOF ON A.MOBILE = CH_SOF.MOBILE AND A.MONTH = CH_SOF.MONTH
    -- LEFT JOIN GENERATE_MONTH ON A.MOBILE = GENERATE_MONTH.MOBILE 
    -- AND A.MONTH BETWEEN DATE_SUB(GENERATE_MONTH.TARGET_MONTH, INTERVAL 6 MONTH) AND DATE_SUB(GENERATE_MONTH.TARGET_MONTH, INTERVAL 1 MONTH)
    -- WHERE GENERATE_MONTH.TARGET_MONTH IS NOT NULL
    WHERE A.MONTH BETWEEN DATE_SUB(GENERATE_MONTH.TARGET_MONTH, INTERVAL 6 MONTH) AND DATE_SUB(GENERATE_MONTH.TARGET_MONTH, INTERVAL 1 MONTH)
  )
  GROUP BY 1,2
)


SELECT
  MAIN.*
  ,VARIETY_6M.* EXCEPT(MOBILE, TARGET_MONTH)
  ,IF(RC_RECENCY IS NULL, -1, RC_RECENCY) AS RC_RECENCY
  ,IF(TARGET_RC.MOBILE IS NOT NULL, 1, 0)  AS RC_FLAG
  ,FIRST_RC.FIRST_RC_MONTH
FROM MAIN 
LEFT JOIN TARGET_RC ON MAIN.MOBILE = TARGET_RC.MOBILE AND MAIN.TARGET_MONTH = TARGET_RC.MONTH
LEFT JOIN VARIETY_6M ON MAIN.MOBILE = VARIETY_6M.MOBILE AND MAIN.TARGET_MONTH = VARIETY_6M.TARGET_MONTH
LEFT JOIN RECENCY_RC ON MAIN.MOBILE = RECENCY_RC.MOBILE AND MAIN.TARGET_MONTH = RECENCY_RC.TARGET_MONTH
LEFT JOIN FIRST_RC ON MAIN.MOBILE = FIRST_RC.MOBILE