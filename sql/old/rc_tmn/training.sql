WITH MAIN AS (
  SELECT *
  FROM `acm-bi.Analysis.SA_20210205_VAS_RECURRING_PREDICTION_DATA`
  WHERE TARGET_MONTH < '2020-10-01'
),
RC AS (
  SELECT *
  FROM MAIN
  WHERE RC_FLAG = 1
),
NONRC AS (
  SELECT A.* EXCEPT(RANK, RND)
  FROM (
    SELECT *, ROW_NUMBER() OVER(PARTITION BY TARGET_MONTH ORDER BY RND) AS RANK
    FROM (
      SELECT *, RAND() AS RND
      FROM MAIN
      WHERE RC_FLAG = 0
    )
  ) A LEFT JOIN (
    SELECT TARGET_MONTH, COUNT(DISTINCT MOBILE) AS USERS
    FROM RC
    GROUP BY 1
  ) B ON A.TARGET_MONTH = B.TARGET_MONTH
  WHERE RANK <= USERS
)

SELECT *
FROM RC
UNION ALL
SELECT *
FROM NONRC