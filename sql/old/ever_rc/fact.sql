WITH MAIN AS (
  SELECT
    MOBILE
    ,COUNT(1) AS TOTAL_MONTH
    ,SUM(TPV) AS TPV_6M
    ,AVG(TPV) AS TPV_1M
    ,AVG(TS) AS TPV_TS
    ,SUM(FREQ) AS FREQ_6M
    ,AVG(FREQ) AS FREQ_1M
    ,SUM(COVER_DAY) AS COVER_DAY_6M
    ,AVG(COVER_DAY) AS COVER_DAY_1M

    --- PACKAGE TYPE 6M
    ,SUM(PT_VOICE_FREQ) AS PT_VOICE_FREQ_6M
    ,SUM(PT_FIX_SPEED_NS_FREQ) AS PT_FIX_SPEED_NS_FREQ_6M
    ,SUM(PT_FIX_SPEED_UL_FREQ) AS PT_FIX_SPEED_UL_FREQ_6M
    ,SUM(PT_FULL_SPEED_NS_FREQ) AS PT_FULL_SPEED_NS_FREQ_6M
    ,SUM(PT_BURST_SPEED_FREQ) AS PT_BURST_SPEED_FREQ_6M
    ,SUM(PT_BUNDLE_APP_FREQ) AS PT_BUNDLE_APP_FREQ_6M
    ,SUM(PT_OTHER_FREQ) AS PT_OTHER_FREQ_6M

    --- PACKAGE TYPE 1M
    ,AVG(PT_VOICE_FREQ) AS PT_VOICE_FREQ_1M
    ,AVG(PT_FIX_SPEED_NS_FREQ) AS PT_FIX_SPEED_NS_FREQ_1M
    ,AVG(PT_FIX_SPEED_UL_FREQ) AS PT_FIX_SPEED_UL_FREQ_1M
    ,AVG(PT_FULL_SPEED_NS_FREQ) AS PT_FULL_SPEED_NS_FREQ_1M
    ,AVG(PT_BURST_SPEED_FREQ) AS PT_BURST_SPEED_FREQ_1M
    ,AVG(PT_BUNDLE_APP_FREQ) AS PT_BUNDLE_APP_FREQ_1M

    --- PACKAGE TYPE COVER 6M
    ,SUM(PT_VOICE_COVER) AS PT_VOICE_COVER_6M
    ,SUM(PT_FIX_SPEED_NS_COVER) AS PT_FIX_SPEED_NS_COVER_6M
    ,SUM(PT_FIX_SPEED_UL_COVER) AS PT_FIX_SPEED_UL_COVER_6M
    ,SUM(PT_FULL_SPEED_NS_COVER) AS PT_FULL_SPEED_NS_COVER_6M
    ,SUM(PT_BURST_SPEED_COVER) AS PT_BURST_SPEED_COVER_6M
    ,SUM(PT_BUNDLE_APP_COVER) AS PT_BUNDLE_APP_COVER_6M
    ,SUM(PT_OTHER_COVER) AS PT_OTHER_COVER_6M

    --- PACKAGE TYPE COVER 1M
    ,AVG(PT_VOICE_COVER) AS PT_VOICE_COVER_1M
    ,AVG(PT_FIX_SPEED_NS_COVER) AS PT_FIX_SPEED_NS_COVER_1M
    ,AVG(PT_FIX_SPEED_UL_COVER) AS PT_FIX_SPEED_UL_COVER_1M
    ,AVG(PT_FULL_SPEED_NS_COVER) AS PT_FULL_SPEED_NS_COVER_1M
    ,AVG(PT_BURST_SPEED_COVER) AS PT_BURST_SPEED_COVER_1M
    ,AVG(PT_BUNDLE_APP_COVER) AS PT_BUNDLE_APP_COVER_1M

    --- PACKAGE VALIDITY 6M
    ,SUM(V_1D_FREQ) AS V_1D_FREQ_6M
    ,SUM(V_2D_FREQ) AS V_2D_FREQ_6M
    ,SUM(V_3D_FREQ) AS V_3D_FREQ_6M
    ,SUM(V_5D_FREQ) AS V_5D_FREQ_6M
    ,SUM(V_7D_FREQ) AS V_7D_FREQ_6M
    ,SUM(V_8D_FREQ) AS V_8D_FREQ_6M
    ,SUM(V_10D_FREQ) AS V_10D_FREQ_6M
    ,SUM(V_15D_FREQ) AS V_15D_FREQ_6M
    ,SUM(V_30D_FREQ) AS V_30D_FREQ_6M
    ,SUM(V_90D_FREQ) AS V_90D_FREQ_6M
    ,SUM(V_180D_FREQ) AS V_180D_FREQ_6M
    ,SUM(V_OTHER_FREQ) AS V_OTHER_FREQ_6M

    --- PACKAGE VALIDITY 1M
    ,AVG(V_1D_FREQ) AS V_1D_FREQ_1M
    ,AVG(V_2D_FREQ) AS V_2D_FREQ_1M
    ,AVG(V_3D_FREQ) AS V_3D_FREQ_1M
    ,AVG(V_5D_FREQ) AS V_5D_FREQ_1M
    ,AVG(V_7D_FREQ) AS V_7D_FREQ_1M
    ,AVG(V_8D_FREQ) AS V_8D_FREQ_1M
    ,AVG(V_10D_FREQ) AS V_10D_FREQ_1M
    ,AVG(V_15D_FREQ) AS V_15D_FREQ_1M
    ,AVG(V_30D_FREQ) AS V_30D_FREQ_1M
    ,AVG(V_90D_FREQ) AS V_90D_FREQ_1M
    ,AVG(V_180D_FREQ) AS V_180D_FREQ_1M
    ,AVG(V_OTHER_FREQ) AS V_OTHER_FREQ_1M

  FROM (
    SELECT
      DATE_TRUNC(DATE(TRANSACTION_DATETIME),MONTH) AS MONTH
      ,MOBILE
      ,SUM(VAS_PACKAGE_AMOUNT) AS TPV
      ,AVG(VAS_PACKAGE_AMOUNT) AS TS
      ,COUNT(1) AS FREQ
      ,SUM(VALIDITY) AS COVER_DAY

      --- PACKAGE TYPE
      ,SUM(PT_VOICE) AS PT_VOICE_FREQ
      ,SUM(PT_FIX_SPEED_NS) AS PT_FIX_SPEED_NS_FREQ
      ,SUM(PT_FIX_SPEED_UL) AS PT_FIX_SPEED_UL_FREQ
      ,SUM(PT_FULL_SPEED_NS) AS PT_FULL_SPEED_NS_FREQ
      ,SUM(PT_BURST_SPEED) AS PT_BURST_SPEED_FREQ
      ,SUM(PT_BUNDLE_APP) AS PT_BUNDLE_APP_FREQ
      ,SUM(PT_OTHER) AS PT_OTHER_FREQ

      ,SUM(IF(PT_VOICE = 1, VALIDITY, 0)) AS PT_VOICE_COVER
      ,SUM(IF(PT_FIX_SPEED_NS = 1, VALIDITY, 0)) AS PT_FIX_SPEED_NS_COVER
      ,SUM(IF(PT_FIX_SPEED_UL = 1, VALIDITY, 0)) AS PT_FIX_SPEED_UL_COVER
      ,SUM(IF(PT_FULL_SPEED_NS = 1, VALIDITY, 0)) AS PT_FULL_SPEED_NS_COVER
      ,SUM(IF(PT_BURST_SPEED = 1, VALIDITY, 0)) AS PT_BURST_SPEED_COVER
      ,SUM(IF(PT_BUNDLE_APP = 1, VALIDITY, 0)) AS PT_BUNDLE_APP_COVER
      ,SUM(IF(PT_OTHER = 1, VALIDITY, 0)) AS PT_OTHER_COVER

      --- PACKAGE VALIDITY
      ,SUM(V_1D) AS V_1D_FREQ
      ,SUM(V_2D) AS V_2D_FREQ
      ,SUM(V_3D) AS V_3D_FREQ
      ,SUM(V_5D) AS V_5D_FREQ
      ,SUM(V_7D) AS V_7D_FREQ
      ,SUM(V_8D) AS V_8D_FREQ
      ,SUM(V_10D) AS V_10D_FREQ
      ,SUM(V_15D) AS V_15D_FREQ
      ,SUM(V_30D) AS V_30D_FREQ
      ,SUM(V_90D) AS V_90D_FREQ
      ,SUM(V_180D) AS V_180D_FREQ
      ,SUM(V_OTHER) AS V_OTHER_FREQ

    FROM (
      SELECT
        TRANSACTION_DATETIME
        ,MOBILE
        ,VAS_PACKAGE_AMOUNT
        ,VAS_PACKAGE_TYPE
        ,CASE
          WHEN VAS_PACKAGE_VALIDITY IN ('1D','24H') THEN 1
          WHEN VAS_PACKAGE_VALIDITY IN ('2D','48H') THEN 2
          WHEN VAS_PACKAGE_VALIDITY IN ('3D','72H') THEN 3
          WHEN VAS_PACKAGE_VALIDITY IS NULL THEN -1
          ELSE CAST(REGEXP_EXTRACT(VAS_PACKAGE_VALIDITY , '^[0-9]+') AS INT64)
        END AS VALIDITY
        --- PACKAGE TYPE
        ,IF(VAS_PACKAGE_TYPE = 'FULL_SPEED_NS', 1, 0) AS PT_FULL_SPEED_NS
        ,IF(VAS_PACKAGE_TYPE = 'VOICE', 1, 0) AS PT_VOICE
        ,IF(VAS_PACKAGE_TYPE = 'FIX_SPEED_NS', 1, 0) AS PT_FIX_SPEED_NS
        ,IF(VAS_PACKAGE_TYPE = 'FIX_SPEED_UL', 1, 0) AS PT_FIX_SPEED_UL
        ,IF(VAS_PACKAGE_TYPE = 'BURST_SPEED', 1, 0) AS PT_BURST_SPEED
        ,IF(VAS_PACKAGE_TYPE = 'BUNDLE_APP', 1, 0) AS PT_BUNDLE_APP
        ,IF(VAS_PACKAGE_TYPE = 'OTHER', 1, 0) AS PT_OTHER

        --- PACKAGE VALIDITY
        ,IF(VAS_PACKAGE_VALIDITY IN ('1D','24H'), 1, 0) AS V_1D
        ,IF(VAS_PACKAGE_VALIDITY IN ('2D','48H'), 1, 0) AS V_2D
        ,IF(VAS_PACKAGE_VALIDITY IN ('3D','72H'), 1, 0) AS V_3D
        ,IF(VAS_PACKAGE_VALIDITY IN ('5D'), 1, 0) AS V_5D
        ,IF(VAS_PACKAGE_VALIDITY IN ('7D'), 1, 0) AS V_7D
        ,IF(VAS_PACKAGE_VALIDITY IN ('8D'), 1, 0) AS V_8D
        ,IF(VAS_PACKAGE_VALIDITY IN ('10D'), 1, 0) AS V_10D
        ,IF(VAS_PACKAGE_VALIDITY IN ('15D'), 1, 0) AS V_15D
        ,IF(VAS_PACKAGE_VALIDITY IN ('30D'), 1, 0) AS V_30D
        ,IF(VAS_PACKAGE_VALIDITY IN ('90D'), 1, 0) AS V_90D
        ,IF(VAS_PACKAGE_VALIDITY IN ('180D'), 1, 0) AS V_180D
        ,IF(VAS_PACKAGE_VALIDITY IS NULL OR VAS_PACKAGE_VALIDITY IN ('6D','240D','365D'), 1, 0) AS V_OTHER

      FROM `acm-prod.datamart.DM_VAS_TOPUP_ALL_CHANNEL_TRANSACTION` 
      WHERE 1=1
        AND DATE_TRUNC(DATE(TRANSACTION_DATETIME),MONTH) BETWEEN '2020-07-01' AND '2020-12-01'
        AND TRMV_TYPE = 'PRE'
    )
    WHERE 1=1 
    AND VALIDITY <> -1
    AND VAS_PACKAGE_TYPE IS NOT NULL
    GROUP BY 1,2
  )
  GROUP BY 1
),
RC AS (
  SELECT
    MOBILE
    ,MIN(DATE(TRANSACTION_DATETIME)) AS FIRST_DATE
    ,MAX(DATE(TRANSACTION_DATETIME)) AS LAST_DATE
    ,MIN(DATE_DIFF(DATE('2020-12-01'), DATE_TRUNC(DATE(TRANSACTION_DATETIME),MONTH), MONTH)) AS RECENCY_MONTH
  FROM `acm-prod.datamart.DM_VAS_TOPUP_ALL_CHANNEL_TRANSACTION` 
  WHERE TRMV_TYPE = 'PRE'
  AND VAS_PACKAGE_CODE LIKE '%RC%'
  AND DATE_TRUNC(DATE(TRANSACTION_DATETIME),MONTH) <= '2020-12-01'
  GROUP BY 1
)

SELECT MAIN.*, RC.* EXCEPT(MOBILE)
FROM MAIN
LEFT JOIN RC ON MAIN.MOBILE = RC.MOBILE