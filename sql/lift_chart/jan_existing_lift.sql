SELECT
  DECILE_GROUP
  ,COUNT(DISTINCT MOBILE) AS TOTAL
  ,COUNT(DISTINCT IF(RC_FLAG=1,MOBILE,NULL)) AS CONVERT
FROM (
  SELECT
    *
    ,CASE
      WHEN RANK <= D1 THEN 1
      WHEN RANK <= D2 THEN 2
      WHEN RANK <= D3 THEN 3
      WHEN RANK <= D4 THEN 4
      WHEN RANK <= D5 THEN 5
      WHEN RANK <= D6 THEN 6
      WHEN RANK <= D7 THEN 7
      WHEN RANK <= D8 THEN 8
      WHEN RANK <= D9 THEN 9
      ELSE 10
    END AS DECILE_GROUP
  FROM (
    SELECT
      *
      ,PERCENTILE_CONT(RANK,0.1) OVER() AS D1
      ,PERCENTILE_CONT(RANK,0.2) OVER() AS D2
      ,PERCENTILE_CONT(RANK,0.3) OVER() AS D3
      ,PERCENTILE_CONT(RANK,0.4) OVER() AS D4
      ,PERCENTILE_CONT(RANK,0.5) OVER() AS D5
      ,PERCENTILE_CONT(RANK,0.6) OVER() AS D6
      ,PERCENTILE_CONT(RANK,0.7) OVER() AS D7
      ,PERCENTILE_CONT(RANK,0.8) OVER() AS D8
      ,PERCENTILE_CONT(RANK,0.9) OVER() AS D9
    FROM (
      SELECT
        *
        ,ROW_NUMBER() OVER(ORDER BY PROB DESC) AS RANK
      FROM `acm-bi.Analysis.SA_VAS_RECURRING_EXISTING_JAN21_RESULT`
      ORDER BY PROB
    )
  )
)
GROUP BY 1
ORDER BY 1

--- FILTER BY BIZ RULE
SELECT
  DECILE_GROUP
  ,COUNT(DISTINCT MOBILE) AS TOTAL
  ,COUNT(DISTINCT IF(RC_FLAG=1,MOBILE,NULL)) AS CONVERT
FROM (
  SELECT
    *
    ,CASE
      WHEN RANK <= D1 THEN 1
      WHEN RANK <= D2 THEN 2
      WHEN RANK <= D3 THEN 3
      WHEN RANK <= D4 THEN 4
      WHEN RANK <= D5 THEN 5
      WHEN RANK <= D6 THEN 6
      WHEN RANK <= D7 THEN 7
      WHEN RANK <= D8 THEN 8
      WHEN RANK <= D9 THEN 9
      ELSE 10
    END AS DECILE_GROUP
  FROM (
    SELECT
      *
      ,PERCENTILE_CONT(RANK,0.1) OVER() AS D1
      ,PERCENTILE_CONT(RANK,0.2) OVER() AS D2
      ,PERCENTILE_CONT(RANK,0.3) OVER() AS D3
      ,PERCENTILE_CONT(RANK,0.4) OVER() AS D4
      ,PERCENTILE_CONT(RANK,0.5) OVER() AS D5
      ,PERCENTILE_CONT(RANK,0.6) OVER() AS D6
      ,PERCENTILE_CONT(RANK,0.7) OVER() AS D7
      ,PERCENTILE_CONT(RANK,0.8) OVER() AS D8
      ,PERCENTILE_CONT(RANK,0.9) OVER() AS D9
    FROM (
      SELECT
        *
        ,ROW_NUMBER() OVER(ORDER BY PROB DESC) AS RANK
      FROM `acm-bi.Analysis.SA_VAS_RECURRING_EXISTING_JAN21_RESULT`
      WHERE 1=1
      AND MOBILE IN (
        SELECT DISTINCT MOBILE
        FROM `acm-prod.datamart.DM_VAS_TOPUP_ALL_CHANNEL_TRANSACTION`
        WHERE VAS_PACKAGE_CODE LIKE '%OC%'
        AND DATE_TRUNC(DATE(TRANSACTION_DATETIME),MONTH) BETWEEN '2020-11-01' AND '2020-12-01'
      )
      AND MOBILE NOT IN (
        SELECT DISTINCT MOBILE
        FROM `acm-prod.datamart.DM_VAS_TOPUP_ALL_CHANNEL_TRANSACTION`
        WHERE VAS_PACKAGE_CODE LIKE '%RC%'
        AND DATE_TRUNC(DATE(TRANSACTION_DATETIME),MONTH) BETWEEN '2020-11-01' AND '2020-12-01'
      )
      AND MOBILE NOT IN (
        SELECT DISTINCT MOBILE
        FROM `acm-prod.diy_standard_sql.DM_VAS_RECURRING`
      )
      AND MOBILE IN (
        SELECT MOBILE
        FROM `acm-prod.datamart_std.DM_V1_MOVEMENT_TRANSACTION`
        WHERE SERVICE_LEVEL1 = 'VAS TOPUP' AND MAU_FLAG = 1
        AND DATE_TRUNC(DATE(TRANS_DATETIME),MONTH) BETWEEN '2020-07-01' AND '2020-12-01'
      )
      ORDER BY PROB
    )
  )
)
GROUP BY 1
ORDER BY 1