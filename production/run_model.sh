#/bin/sh
. ~/.bash_profile

SCRIPT_PATH=/analytic/bitbucket/ascend-server/ModelScheduler
KEY_FILE=$SCRIPT_PATH/acm-bi-gce-instance-service.json

SEND_MESSAGE=/analytic/bitbucket/ascend-server/Hangout/send_message_testbot.sh

## ---------------------------------------------------------------------------------------------------------
## Authen
## ---------------------------------------------------------------------------------------------------------

gcloud auth activate-service-account --key-file=${KEY_FILE}

## ---------------------------------------------------------------------------------------------------------
## Start acm-bi-dev-machine-02
## ---------------------------------------------------------------------------------------------------------

gcloud compute instances start acm-bi-dev-machine-02 --zone=us-central1-c

## ---------------------------------------------------------------------------------------------------------
## Check Status acm-bi-dev-machine-02
## ---------------------------------------------------------------------------------------------------------

STATUS=`gcloud compute instances describe acm-bi-dev-machine-02 --zone=us-central1-c|grep status|awk '{print $2}'`

echo "STATUS : $STATUS"

if [ $STATUS == RUNNING ]; then
        echo "Started Completed."
else
        echo "Started Failed."
        exit 99
fi


## ---------------------------------------------------------------------------------------------------------
## Run Model
## ---------------------------------------------------------------------------------------------------------

/bin/sh $SCRIPT_PATH/run_model_bitbucket.sh ValueSegment_Monthly.py > $SCRIPT_PATH/ValueSegment_Monthly.out
/bin/sh $SEND_MESSAGE "`tail -1 $SCRIPT_PATH/ValueSegment_Monthly.out`"
if [ $? -ne 0 ]; then
        exit 99
fi

## ---------------------------------------------------------------------------------------------------------
## Stop acm-bi-dev-machine-02
## ---------------------------------------------------------------------------------------------------------

gcloud compute instances stop acm-bi-dev-machine-02 --zone=us-central1-c

STATUS=`gcloud compute instances describe acm-bi-dev-machine-02 --zone=us-central1-c|grep status|awk '{print $2}'`

echo "STATUS : $STATUS"

if [ $STATUS == RUNNING ]; then
        echo "Stoped Failed."
else
        echo "Stoped Completed."