import pandas as pd
import pickle
import numpy as np
from numpy import sort
from sklearn.model_selection import train_test_split
from sklearn.feature_selection import SelectFromModel
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score
from sklearn.utils import shuffle, resample
import scikitplot as skplt
import seaborn as sns
import matplotlib.pyplot as plt
import xgboost as xgb
import os
from datetime import datetime, timedelta
from gbq import gbq_insert, gbq_query, gbq_delete

FACT_TABLE = 'acm-bi.BI_TARGET_LIST.SA_VAS_RECURRING_PREDICTION_FACT'
RESULT_TABLE = 'acm-bi.BI_TARGET_LIST.SA_VAS_RECURRING_PREDICTION_RESULT'

def replace_fact(fact_table):
    delete_sql = f'DELETE {fact_table} WHERE 1=1'
    sql = '''
        WITH GENERATE_MONTH AS (
        SELECT DATE_TRUNC(CURRENT_DATE(),MONTH) AS TARGET_MONTH 
        UNION ALL SELECT DATE_SUB(DATE_TRUNC(CURRENT_DATE(),MONTH),INTERVAL 1 MONTH)
        ),
        CH_SOF AS (
        SELECT
            DISTINCT 
            DATE_TRUNC(DATE(TRANS_DATETIME),MONTH) AS MONTH
            ,MOBILE
            --- SOF
            ,SUM(SOF_WALLET) AS SOF_WALLET
            ,SUM(SOF_AIRTIME) AS SOF_AIRTIME

            --- BIZ CH
            ,SUM(CH_WALLET) AS CH_WALLET
            ,SUM(CH_WEB) AS CH_WEB
        FROM (
            SELECT 
            TRANS_DATETIME
            ,COALESCE(MOBILE_RECEIVER,MOBILE_BUYER) AS MOBILE
            ,SOURCEOFFUND
            ,IF(SOURCEOFFUND = 'E-WALLET', 1, 0) AS SOF_WALLET
            ,IF(SOURCEOFFUND = 'AIRTIME', 1, 0) AS SOF_AIRTIME
            ,IF(BIZ_CHANNEL = 'TMN WEB', 1, 0) AS CH_WEB
            ,IF(BIZ_CHANNEL = 'TMN WALLET', 1, 0) AS CH_WALLET
            FROM `acm-prod.datamart.DM_VAS_TOPUP_TRANSACTION`
            WHERE 1=1
            AND DATE_TRUNC(DATE(TRANS_DATETIME),MONTH) >= '2020-10-01'
            AND TRMV_TYPE = 'PRE'
            AND MERCHANT_ID NOT LIKE '%68%'
        )
        GROUP BY 1,2
        ),
        TARGET_RC AS (
        SELECT DISTINCT DATE_TRUNC(DATE(TRANSACTION_DATETIME),MONTH) AS MONTH, MOBILE
        FROM `acm-prod.datamart.DM_VAS_TOPUP_ALL_CHANNEL_TRANSACTION`,GENERATE_MONTH
        WHERE VAS_PACKAGE_CODE LIKE '%_RC_%'
        ),
        RECENCY_RC AS (
        SELECT 
            MOBILE
            ,TARGET_MONTH
            ,MIN(DATE_DIFF(TARGET_MONTH,MONTH,MONTH)) AS RC_RECENCY
            ,MAX(MONTH) AS LAST_RC
        FROM (
            SELECT DISTINCT DATE_TRUNC(DATE(TRANSACTION_DATETIME),MONTH) AS MONTH, MOBILE
            FROM `acm-prod.datamart.DM_VAS_TOPUP_ALL_CHANNEL_TRANSACTION`
            WHERE VAS_PACKAGE_CODE LIKE '%_RC_%'
        ), GENERATE_MONTH
        WHERE TARGET_MONTH > MONTH
        GROUP BY 1,2
        ),
        VARIETY_6M AS (
        SELECT 
            MOBILE
            ,TARGET_MONTH
            ,COUNT(DISTINCT VAS_PACKAGE_CODE) AS V_CODE_6M
            ,COUNT(DISTINCT VAS_PACKAGE_TYPE) AS V_TYPE_6M
            ,COUNT(DISTINCT VAS_PACKAGE_VALIDITY) AS V_VALIDITY_6M
        FROM `acm-prod.datamart.DM_VAS_TOPUP_ALL_CHANNEL_TRANSACTION`, GENERATE_MONTH
        WHERE DATE_TRUNC(DATE(TRANSACTION_DATETIME),MONTH) BETWEEN DATE_SUB(GENERATE_MONTH.TARGET_MONTH, INTERVAL 6 MONTH) AND DATE_SUB(GENERATE_MONTH.TARGET_MONTH, INTERVAL 1 MONTH)
        AND TRMV_TYPE = 'PRE'
        GROUP BY 1,2
        ),
        SIM_AGE AS (
        SELECT  TMH_MSISDN AS MOBILE,MAX(TMH_ACTIVATE_DATE) AS TMH_ACTIVATE_DATE
        FROM `acm-prod.datamart.DM_TRUEMOVEH_TMW`
        WHERE TMH_ACTIVATE_DATE IS NOT NULL
        -- AND TMH_CUSTTYPE = 'PRE'
        GROUP BY 1
        ),
        MAIN AS (
        SELECT
            MOBILE
            ,TARGET_MONTH
            ,COUNT(1) AS TOTAL_MONTH
            ,SUM(TPV) AS TPV_6M
            ,SUM(TPV)/SUM(FREQ) AS TS
            ,SUM(COVER_DAY) AS COVER_DAY_6M
            ,SUM(PT_FIX_SPEED_NS_COVER) AS PT_FIX_SPEED_NS_COVER_6M
            ,SUM(PT_FIX_SPEED_UL_COVER) AS PT_FIX_SPEED_UL_COVER_6M
            ,SUM(SOF_OTHER_FREQ) AS SOF_OTHER_FREQ_6M
        FROM (
            SELECT 
            A.*
            ,TARGET_MONTH
            --- SOF
            ,COALESCE(SOF_WALLET,0) AS SOF_WALLET_FREQ
            ,COALESCE(SOF_AIRTIME,0) AS SOF_AIRTIME_FREQ
            ,IF(FREQ-COALESCE(SOF_WALLET,0)-COALESCE(SOF_AIRTIME,0) < 0, 0, FREQ-COALESCE(SOF_WALLET,0)-COALESCE(SOF_AIRTIME,0)) AS SOF_OTHER_FREQ
            --- CH
            ,COALESCE(CH_WALLET,0) AS CH_WALLET_FREQ
            ,COALESCE(CH_WEB,0) AS CH_WEB_FREQ
            ,IF(FREQ-COALESCE(CH_WALLET,0)-COALESCE(CH_WEB,0) < 0, 0, FREQ-COALESCE(CH_WALLET,0)-COALESCE(CH_WEB,0)) AS CH_OTHER_FREQ

            FROM (
            SELECT
                DATE_TRUNC(DATE(TRANSACTION_DATETIME),MONTH) AS MONTH
                ,ALLCH.MOBILE
                ,SUM(VAS_PACKAGE_AMOUNT) AS TPV
                ,AVG(VAS_PACKAGE_AMOUNT) AS TS
                ,COUNT(1) AS FREQ
                ,SUM(VALIDITY) AS COVER_DAY
                ,COUNT(DISTINCT VAS_PACKAGE_CODE) AS V_CODE
                ,SUM(IF(PT_FIX_SPEED_NS = 1, VALIDITY, 0)) AS PT_FIX_SPEED_NS_COVER
                ,SUM(IF(PT_FIX_SPEED_UL = 1, VALIDITY, 0)) AS PT_FIX_SPEED_UL_COVER
            FROM (
                SELECT
                TRANSACTION_DATETIME
                ,MOBILE
                ,VAS_PACKAGE_AMOUNT
                ,VAS_PACKAGE_TYPE
                ,VAS_PACKAGE_CODE
                ,VAS_PACKAGE_VALIDITY
                ,CASE
                    WHEN VAS_PACKAGE_VALIDITY IN ('1D','24H') THEN 1
                    WHEN VAS_PACKAGE_VALIDITY IN ('2D','48H') THEN 2
                    WHEN VAS_PACKAGE_VALIDITY IN ('3D','72H') THEN 3
                    WHEN VAS_PACKAGE_VALIDITY IS NULL THEN -1
                    ELSE CAST(REGEXP_EXTRACT(VAS_PACKAGE_VALIDITY , '^[0-9]+') AS INT64)
                END AS VALIDITY
                --- PACKAGE TYPE
                ,IF(VAS_PACKAGE_TYPE = 'FIX_SPEED_NS', 1, 0) AS PT_FIX_SPEED_NS
                ,IF(VAS_PACKAGE_TYPE = 'FIX_SPEED_UL', 1, 0) AS PT_FIX_SPEED_UL
                FROM `acm-prod.datamart.DM_VAS_TOPUP_ALL_CHANNEL_TRANSACTION`
                WHERE 1=1
                AND DATE_TRUNC(DATE(TRANSACTION_DATETIME),MONTH) >= '2020-10-01'
                AND TRMV_TYPE = 'PRE'
            ) ALLCH
            WHERE 1=1 
            AND VALIDITY <> -1
            AND VAS_PACKAGE_TYPE IS NOT NULL
            GROUP BY 1,2
            ) A,GENERATE_MONTH
            LEFT JOIN CH_SOF ON A.MOBILE = CH_SOF.MOBILE AND A.MONTH = CH_SOF.MONTH
            WHERE A.MONTH BETWEEN DATE_SUB(GENERATE_MONTH.TARGET_MONTH, INTERVAL 6 MONTH) AND DATE_SUB(GENERATE_MONTH.TARGET_MONTH, INTERVAL 1 MONTH)
        )
        GROUP BY 1,2
        )

        SELECT
        MAIN.MOBILE
        ,MAIN.TARGET_MONTH
        ,TPV_6M
        ,TS
        ,IF(COVER_DAY_6M>180,180,COVER_DAY_6M) AS COVER_DAY_6M
        ,IF(PT_FIX_SPEED_UL_COVER_6M>180,180,PT_FIX_SPEED_UL_COVER_6M) AS PT_FIX_SPEED_UL_COVER_6M
        ,TOTAL_MONTH
        ,SOF_OTHER_FREQ_6M
        ,IF(PT_FIX_SPEED_NS_COVER_6M>180,180,PT_FIX_SPEED_NS_COVER_6M) AS PT_FIX_SPEED_NS_COVER_6M
        ,VARIETY_6M.V_CODE_6M
        ,IF(DATE_DIFF(MAIN.TARGET_MONTH, DATE(SIM_AGE.TMH_ACTIVATE_DATE), MONTH) IS NULL,1,DATE_DIFF(MAIN.TARGET_MONTH, DATE(SIM_AGE.TMH_ACTIVATE_DATE), MONTH)) AS SIM_MONTH_AGE
        ,IF(RC_RECENCY IS NULL, -1, RC_RECENCY) AS RC_RECENCY
        ,IF(TARGET_RC.MOBILE IS NOT NULL, 1, 0)  AS RC_FLAG
        FROM MAIN 
        LEFT JOIN TARGET_RC ON MAIN.MOBILE = TARGET_RC.MOBILE AND MAIN.TARGET_MONTH = TARGET_RC.MONTH
        LEFT JOIN VARIETY_6M ON MAIN.MOBILE = VARIETY_6M.MOBILE AND MAIN.TARGET_MONTH = VARIETY_6M.TARGET_MONTH
        LEFT JOIN RECENCY_RC ON MAIN.MOBILE = RECENCY_RC.MOBILE AND MAIN.TARGET_MONTH = RECENCY_RC.TARGET_MONTH
        LEFT JOIN SIM_AGE ON MAIN.MOBILE = SIM_AGE.MOBILE
    '''
    gbq_delete(delete_sql)
    gbq_insert(sql, fact_table)


def load_data(df,balance=False):
    df = shuffle(df)
    
    if balance == True:
        df_majority = df[df['RC_FLAG'] == 0]
        df_minority = df[df['RC_FLAG'] == 1]
        
        df_majority_downsampled = resample(df_majority,replace=False,n_samples=len(df_minority),random_state=123)

        df = pd.concat([df_majority_downsampled, df_minority])
    X = df.drop([
        'RC_FLAG'
        ,'TARGET_MONTH'
        ,'MOBILE'
        ,'RC_RECENCY'
    ],axis=1)
    
    y = df[['RC_FLAG']]
    mobile = df[['MOBILE']]
    return X, y, mobile

def training():
    dt = datetime.now()
    date = (dt.replace(day=1) - timedelta(days=1)).replace(day=1).strftime('%Y-%m-%d')
    sql = f'''
        SELECT
            MOBILE
            ,TARGET_MONTH
            ,TPV_6M
            ,TS
            ,COVER_DAY_6M
            ,PT_FIX_SPEED_UL_COVER_6M
            ,TOTAL_MONTH
            ,SOF_OTHER_FREQ_6M
            ,PT_FIX_SPEED_NS_COVER_6M
            ,V_CODE_6M
            ,RC_FLAG
            ,RC_RECENCY
            ,SIM_MONTH_AGE
        FROM `acm-bi.BI_TARGET_LIST.SA_VAS_RECURRING_PREDICTION_FACT`
        WHERE RC_RECENCY <> -1
        AND TARGET_MONTH = '{date}'
        LIMIT 10000
    '''
    _, df = gbq_query(sql)
    X,y,mobile = load_data(df)

    model = xgb.XGBClassifier(importance_type='weight')
    model.fit(X, y)

    return model

def predict(user_type, model, result_table):
    dt = datetime.now()
    date = dt.replace(day=1).strftime('%Y-%m-%d')

    recency = '<> -1'
    if user_type == 'NEW RC':
        recency = '= -1'
    sql = f'''
        SELECT
            MOBILE
            ,TARGET_MONTH
            ,TPV_6M
            ,TS
            ,COVER_DAY_6M
            ,PT_FIX_SPEED_UL_COVER_6M
            ,TOTAL_MONTH
            ,SOF_OTHER_FREQ_6M
            ,PT_FIX_SPEED_NS_COVER_6M
            ,V_CODE_6M
            ,RC_FLAG
            ,RC_RECENCY
            ,SIM_MONTH_AGE
        FROM `acm-bi.BI_TARGET_LIST.SA_VAS_RECURRING_PREDICTION_FACT`
        WHERE RC_RECENCY {recency}
        AND TARGET_MONTH = '{date}'
        LIMIT 10000
    '''
    _, df = gbq_query(sql)
    prob_result = model.predict_proba(df.drop(['MOBILE','TARGET_MONTH','RC_FLAG','RC_RECENCY'],axis=1))
    df['SCORE'] = prob_result[:,1:]

    conditions = [
        (df['SCORE'] <= 0.3),
        (df['SCORE'] > 0.3) & (df['SCORE'] <= 0.7),
        (df['SCORE'] > 0.7) & (df['SCORE'] <= 1)
    ]
    values = ['LOW', 'MEDIUM', 'HIGH']
    df['SCORE_TYPE'] = np.select(conditions, values)
    df = df.assign(USER_TYPE=user_type)
    df = df.assign(MONTH=date)

    result_table_split = result_table.replace('`','').split('.')
    table = result_table_split[1]+'.'+result_table_split[2]
    project_id = result_table_split[0]
    df.to_gbq(table, project_id=project_id, if_exists='append')   

def main():
    print('RUNNING REPLACE RECURRING FACT')
    # replace_fact(FACT_TABLE)
    print('FINISH REPLACE RECURRING FACT')

    print('START TRAINING MODEL')
    model = training()
    print('FINISH TRAINING MODEL')

    print('PREDICT EXISTING RC')
    predict('EXISTING RC', model, RESULT_TABLE)
    print('PREDICT NEW RC')
    predict('NEW RC', model, RESULT_TABLE)

if __name__ == "__main__":
    main()