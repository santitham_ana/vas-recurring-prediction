MODEL_PATH="/data/bitbucket/ascend-server/Model"

MODEL_SCRIPT=$1
MODEL_PARAM1=$2
MODEL_PARAM2=$3

KEY_FILE=/analytic/bitbucket/ascend-server/ModelScheduler/acm-bi-gce-instance-service.json
##KEY_FILE=acm-bi-gce-instance-service.json


gcloud auth activate-service-account --key-file=${KEY_FILE}

STATUS=`gcloud compute instances describe acm-bi-dev-machine-02 --zone=us-central1-c|grep status|awk '{print $2}'`

if [ $STATUS == RUNNING ]; then
        echo "$(date) : Server acm-bi-dev-machine-02 Ready : Start run ${MODEL_SCRIPT}"
else
        echo "$(date) : Server acm-bi-dev-machine-02 not start : Start run ${MODEL_SCRIPT} Failed."
        exit 99
fi

gcloud compute ssh pongsiritrivittayasil@acm-bi-dev-machine-02 --zone=us-central1-c --command="sudo su - pongsiri_tri -c \"cd ${MODEL_PATH};python ${MODEL_PATH}/${MODEL_SCRIPT} ${MODEL_PARAM1} ${MODEL_PARAM2}\""

if [ $? -eq 0 ]; then
        echo "$(date) : ${MODEL_SCRIPT} ${MODEL_PARAM1} ${MODEL_PARAM2} : Success"
else
        echo "$(date) : ${MODEL_SCRIPT} ${MODEL_PARAM1} ${MODEL_PARAM2} : Failed!!"
        exit 99
fi