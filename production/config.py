#!/usr/bin/env python
# coding: utf-8

# In[1]:


def init():
    
    global project
    project = 'acm-bi'
    
    global credentials
    credentials = 'credentials/bi-data-service.json'
    
    import os
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = credentials


# In[ ]:




