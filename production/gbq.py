#!/usr/bin/env python
# coding: utf-8

# In[86]:
import config
config.init()

from google.cloud import bigquery
import pandas as pd

# In[87]:


def gbq_insert(sql,dest_table):
    try:
        client = bigquery.Client(project=config.project)
        insert_sql = "INSERT INTO `"+dest_table+"` "+sql
        job_config = bigquery.QueryJobConfig(use_legacy_sql=False)
        query_job = client.query(insert_sql, job_config=job_config)
        query_job.result()
        return True
    except Exception as e:
        print('Function gbq_insert error :',e)
        return False


# In[88]:


def gbq_query(sql):
    try:
        client = bigquery.Client(project=config.project)
        job_config = bigquery.QueryJobConfig(use_legacy_sql=False)
        query_job = client.query(sql, job_config=job_config)
        result = query_job.result()
        return True,result.to_dataframe()
    except Exception as e:
        print('Function gbq_query error :',e)
        return False,e


# In[89]:


def gbq_delete(sql):
    try:
        client = bigquery.Client(project=config.project)
        job_config = bigquery.QueryJobConfig(use_legacy_sql=False)
        query_job = client.query(sql, job_config=job_config)
        query_job.result()
        return True
    except Exception as e:
        print('Function gbq_delete error :',e)
        return False
# In[90]:


def export_excel(df,file_name,columns=None):
    try:
        if columns is not None:
            df.columns = columns
        df.to_excel(file_name,index=False)
        return True
    except Exception as e:
        print('Function export_excel error :',e)
        return False


# In[91]:


def export_csv(df,file_name,columns=None):
    try:
        if columns is not None:
            df.columns = columns
        df.to_csv(file_name,index=False)
        return True
    except Exception as e:
        print('Function export_csv error :',e)
        return False
