#!/usr/bin/env python
# coding: utf-8

# # MONTHLY VALUE SEGMENT RUN ON THE 1ST OF EVERYMONTH

# In[1]:


import config
import gbq
config.init()


# ## PMAU

# In[ ]:


gbq.gbq_insert("""WITH TRANSACTION_DATA AS (SELECT a.TMNID, TRANS_DATETIME, AMOUNT, EXTRACT(month FROM TRANS_DATETIME) AS month,
                                  RANK() OVER (PARTITION BY a.TMNID ORDER BY TRANS_DATETIME DESC ) AS rank
                            FROM `acm-prod.datamart_std.DM_V1_MOVEMENT_TRANSACTION` a
                            JOIN `acm-prod.datamart.DM_V1_TMNID_MASTER` b
                                  ON a.tmnid = b.TMNID
                                  AND b.DELETED_DATE IS NULL
                            WHERE DATE(TRANS_DATETIME) BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL -180 day) AND CURRENT_DATE
                                  AND PMAU_FLAG = 1
                           ),
    RECENCY_DATA AS (SELECT tmnid,
                            DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) AS Recency,
                            PERCENT_RANK() OVER(ORDER BY DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) DESC) AS Percent_Rank_Recency
                        FROM TRANSACTION_DATA
                        WHERE rank = 1),
    FREQUENCY_DATA AS (SELECT tmnid,
                              COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month) AS Frequency,
                              PERCENT_RANK() OVER(ORDER BY COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month)) AS Percent_Rank_Frequency
                         FROM TRANSACTION_DATA
                         GROUP BY 1),
    MONETARY_DATA AS (SELECT tmnid,
                            SUM(AMOUNT)/ COUNT(DISTINCT month) AS Monetary,
                            PERCENT_RANK() OVER(ORDER BY SUM(AMOUNT)/ COUNT(DISTINCT month) ) AS Percent_Rank_Monetary
                          FROM TRANSACTION_DATA 
                          GROUP BY 1 ),
    VIEW AS (SELECT DISTINCT a.tmnid, 
                    b.Recency, Percent_Rank_REcency,
                    CASE  WHEN b.Recency < 8 THEN 4
                          WHEN b.Recency BETWEEN 8 AND 20 THEN 3
                          WHEN b.Recency BETWEEN 20 AND 50 THEN 2
                          WHEN b.Recency >50 THEN 1
                    END AS R_SCORE,
                    c.Frequency, Percent_Rank_Frequency,
                    CASE  WHEN Frequency BETWEEN 0 AND 1 THEN 1
                          WHEN Frequency <= 6 THEN 2
                          WHEN Frequency < 12 THEN 3
                          WHEN Frequency >= 12 THEN 4
                    END AS F_SCORE,
                    d.Monetary, Percent_Rank_Monetary,
                    CASE  WHEN Monetary < 200 THEN 1
                          WHEN Monetary BETWEEN 200 AND 700 THEN 2
                          WHEN Monetary BETWEEN 700 AND 2500 THEN 3
                          WHEN Monetary > 2500 THEN 4
                    END AS M_SCORE
                FROM TRANSACTION_DATA a
                LEFT JOIN RECENCY_DATA b
                      ON a.TMNID = b.TMNID
                LEFT JOIN Frequency_DATA c
                      ON a.TMNID = c.TMNID
                LEFT JOIN MONETARY_DATA d
                      ON a.TMNID = d.TMNID ),
    ALL_TRANSACTION AS (SELECT TMNID,
                                CASE  WHEN SERVICE_LEVEL1 = 'RETAIL PAYMENT' AND SERVICE_LEVEL2 = 'ORGANIZED' THEN 'KA'
                                      WHEN SERVICE_LEVEL1 = 'RETAIL PAYMENT' AND SERVICE_LEVEL2 = 'UNORGANIZED' THEN 'LONGTAIL'
                                      WHEN SERVICE_LEVEL1 = 'RETAIL PAYMENT' THEN SERVICE_LEVEL2
                                      WHEN SERVICE_LEVEL1 = 'BILL PAYMENT' AND SERVICE_LEVEL2 = 'NON-TRUE GROUP' THEN 'BILL NON-TRUE GROUP'
                                      WHEN SERVICE_LEVEL1 = 'BILL PAYMENT' AND SERVICE_LEVEL2 = 'TRUE GROUP' THEN 'BILL TRUE GROUP'
                                ELSE SERVICE_LEVEL1 END SERVICE,
                                SUM(amount) AS Total_amount
                          FROM `acm-prod.datamart_std.DM_V1_MOVEMENT_TRANSACTION`
                          WHERE PMAU_FLAG = 1
                            AND DATE(TRANS_DATETIME) BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL -180 day) AND CURRENT_DATE
                          GROUP BY 1,2),
    RANK_DATA AS (SELECT *,
                        ROW_NUMBER() OVER (PARTITION BY tmnid ORDER BY Total_amount DESC ) AS rank
                    FROM ALL_TRANSACTION )

 SELECT DISTINCT CURRENT_DATE as MODEL_MONTH, 'MONTHLY' as RUN_TYPE, 'PMAU' as SERVICE_LEVEL1  ,'PMAU' as  SERVICE_LEVEL2 , a.*,
            CASE  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) = '444' THEN '1_Champion Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('314','324','334','344','414','424','434') THEN '2_Active_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('332', '333', '342', '343', '432', '433', '442', '443') THEN '3_Active_Medium_Value_Sticky_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('312','313','322','323','412','413','422','423') THEN '4_Active_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('311', '321', '331', '341', '411', '421', '431', '441') THEN '5_Active_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('214','224','234','244') THEN '6_Churn_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('212', '213', '222', '223', '232', '233', '242', '243') THEN '7_Churn_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('211','221','231','241') THEN '8_Churn_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('114', '124', '134', '144') THEN '9_Inactive_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('112','113','122','123','132','133','142','143') THEN '10_Inactive_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('121', '131', '141') THEN '11_Inactive_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('111') THEN '12_Inactive_Lowest_Value_Customer'
            END AS Segment
      FROM VIEW a""",'acm-bi.BI_TARGET_LIST.BI_MODEL_VALUE_SEGMENTATION_MASTER')


# ## 7-ELEVEN: ALL CHANNELS

# In[2]:


gbq.gbq_insert("""WITH TRANSACTION_DATA AS (SELECT a.TMNID, TRANS_DATETIME, AMOUNT, EXTRACT(month FROM TRANS_DATETIME) AS month,a.SERVICE_LEVEL1,CHANNEL_APP,
                                  RANK() OVER (PARTITION BY a.TMNID ORDER BY TRANS_DATETIME DESC ) AS rank   
                            FROM `acm-prod.datamart_std.DM_V1_MOVEMENT_TRANSACTION` a
                            JOIN `acm-prod.datamart.DM_V1_TMNID_MASTER` b
                                  ON a.tmnid = b.TMNID
                                  AND b.DELETED_DATE IS NULL
                            WHERE DATE(TRANS_DATETIME) BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL -180 day) AND CURRENT_DATE
                                  AND a.SERVICE_LEVEL1  = '7-ELEVEN'
                                  AND PMAU_FLAG = 1
                           ),
    RECENCY_DATA AS (SELECT tmnid,
                            DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) AS Recency,
                            PERCENT_RANK() OVER(ORDER BY DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) DESC) AS Percent_Rank_Recency
                        FROM TRANSACTION_DATA
                        WHERE rank = 1),
    FREQUENCY_DATA AS (SELECT tmnid,
                              COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month) AS Frequency,
                              PERCENT_RANK() OVER(ORDER BY COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month)) AS Percent_Rank_Frequency
                         FROM TRANSACTION_DATA
                         GROUP BY 1),
    MONETARY_DATA AS (SELECT tmnid,
                            SUM(AMOUNT)/ COUNT(DISTINCT month) AS Monetary,
                            PERCENT_RANK() OVER(ORDER BY SUM(AMOUNT)/ COUNT(DISTINCT month) ) AS Percent_Rank_Monetary
                          FROM TRANSACTION_DATA 
                          GROUP BY 1 ),
    VIEW AS (SELECT DISTINCT SERVICE_LEVEL1,'7-ELEVEN' as SERVICE_LEVEL2 , a.tmnid, 
                    b.Recency, Percent_Rank_REcency,
                    CASE  WHEN b.Recency < 8 THEN 4
                          WHEN b.Recency BETWEEN 8 AND 20 THEN 3
                          WHEN b.Recency BETWEEN 20 AND 50 THEN 2
                          WHEN b.Recency >50 THEN 1
                    END AS R_SCORE,
                    c.Frequency, Percent_Rank_Frequency,
                    CASE  WHEN Frequency BETWEEN 0 AND 1 THEN 1
                          WHEN Frequency < 6 THEN 2
                          WHEN Frequency < 15 THEN 3
                          WHEN Frequency >= 15 THEN 4
                    END AS F_SCORE,
                    d.Monetary, Percent_Rank_Monetary,
                    CASE  WHEN Monetary < 200 THEN 1
                          WHEN Monetary BETWEEN 200 AND 700 THEN 2
                          WHEN Monetary BETWEEN 700 AND 1500 THEN 3
                          WHEN Monetary > 1500 THEN 4
                    END AS M_SCORE
                FROM TRANSACTION_DATA a
                LEFT JOIN RECENCY_DATA b
                      ON a.TMNID = b.TMNID
                LEFT JOIN Frequency_DATA c
                      ON a.TMNID = c.TMNID
                LEFT JOIN MONETARY_DATA d
                      ON a.TMNID = d.TMNID )
        SELECT DISTINCT CURRENT_DATE as MODEL_MONTH,'MONTHLY' as RUN_TYPE, a.*,
            CASE  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) = '444' THEN '1_Champion Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('314','324','334','344','414','424','434') THEN '2_Active_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('332', '333', '342', '343', '432', '433', '442', '443') THEN '3_Active_Medium_Value_Sticky_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('312','313','322','323','412','413','422','423') THEN '4_Active_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('311', '321', '331', '341', '411', '421', '431', '441') THEN '5_Active_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('214','224','234','244') THEN '6_Churn_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('212', '213', '222', '223', '232', '233', '242', '243') THEN '7_Churn_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('211','221','231','241') THEN '8_Churn_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('114', '124', '134', '144') THEN '9_Inactive_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('112','113','122','123','132','133','142','143') THEN '10_Inactive_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('121', '131', '141') THEN '11_Inactive_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('111') THEN '12_Inactive_Lowest_Value_Customer'
            END AS Segment, 
      FROM VIEW a""",'acm-bi.BI_TARGET_LIST.BI_MODEL_VALUE_SEGMENTATION_MASTER')


# ## 7-ELEVEN: 7APP

# In[3]:


gbq.gbq_insert("""WITH TRANSACTION_DATA AS (SELECT a.TMNID, TRANS_DATETIME, AMOUNT, EXTRACT(month FROM TRANS_DATETIME) AS month,a.SERVICE_LEVEL1,CHANNEL_APP,
                                  RANK() OVER (PARTITION BY a.TMNID ORDER BY TRANS_DATETIME DESC ) AS rank   
                            FROM `acm-prod.datamart_std.DM_V1_MOVEMENT_TRANSACTION` a
                            JOIN `acm-prod.datamart.DM_V1_TMNID_MASTER` b
                                  ON a.tmnid = b.TMNID
                                  AND b.DELETED_DATE IS NULL
                            WHERE DATE(TRANS_DATETIME) BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL -180 day) AND CURRENT_DATE
                                  AND a.SERVICE_LEVEL1  = '7-ELEVEN' and a.CHANNEL_APP  = '7APP'
                                  AND PMAU_FLAG = 1
                           ),
    RECENCY_DATA AS (SELECT tmnid,
                            DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) AS Recency,
                            PERCENT_RANK() OVER(ORDER BY DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) DESC) AS Percent_Rank_Recency
                        FROM TRANSACTION_DATA
                        WHERE rank = 1),
    FREQUENCY_DATA AS (SELECT tmnid,
                              COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month) AS Frequency,
                              PERCENT_RANK() OVER(ORDER BY COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month)) AS Percent_Rank_Frequency
                         FROM TRANSACTION_DATA
                         GROUP BY 1),
    MONETARY_DATA AS (SELECT tmnid,
                            SUM(AMOUNT)/ COUNT(DISTINCT month) AS Monetary,
                            PERCENT_RANK() OVER(ORDER BY SUM(AMOUNT)/ COUNT(DISTINCT month) ) AS Percent_Rank_Monetary
                          FROM TRANSACTION_DATA 
                          GROUP BY 1 ),
    VIEW AS (SELECT DISTINCT SERVICE_LEVEL1,CHANNEL_APP as SERVICE_LEVEL2 , a.tmnid, 
                    b.Recency, Percent_Rank_REcency,
                    CASE  WHEN b.Recency < 8 THEN 4
                          WHEN b.Recency BETWEEN 8 AND 20 THEN 3
                          WHEN b.Recency BETWEEN 20 AND 50 THEN 2
                          WHEN b.Recency >50 THEN 1
                    END AS R_SCORE,
                    c.Frequency, Percent_Rank_Frequency,
                    CASE  WHEN Frequency BETWEEN 0 AND 1 THEN 1
                          WHEN Frequency < 6 THEN 2
                          WHEN Frequency < 15 THEN 3
                          WHEN Frequency >= 15 THEN 4
                    END AS F_SCORE,
                    d.Monetary, Percent_Rank_Monetary,
                    CASE  WHEN Monetary < 200 THEN 1
                          WHEN Monetary BETWEEN 200 AND 700 THEN 2
                          WHEN Monetary BETWEEN 700 AND 1500 THEN 3
                          WHEN Monetary > 1500 THEN 4
                    END AS M_SCORE
                FROM TRANSACTION_DATA a
                LEFT JOIN RECENCY_DATA b
                      ON a.TMNID = b.TMNID
                LEFT JOIN Frequency_DATA c
                      ON a.TMNID = c.TMNID
                LEFT JOIN MONETARY_DATA d
                      ON a.TMNID = d.TMNID )
        SELECT DISTINCT CURRENT_DATE as MODEL_MONTH,'MONTHLY' as RUN_TYPE, a.*,
            CASE  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) = '444' THEN '1_Champion Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('314','324','334','344','414','424','434') THEN '2_Active_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('332', '333', '342', '343', '432', '433', '442', '443') THEN '3_Active_Medium_Value_Sticky_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('312','313','322','323','412','413','422','423') THEN '4_Active_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('311', '321', '331', '341', '411', '421', '431', '441') THEN '5_Active_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('214','224','234','244') THEN '6_Churn_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('212', '213', '222', '223', '232', '233', '242', '243') THEN '7_Churn_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('211','221','231','241') THEN '8_Churn_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('114', '124', '134', '144') THEN '9_Inactive_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('112','113','122','123','132','133','142','143') THEN '10_Inactive_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('121', '131', '141') THEN '11_Inactive_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('111') THEN '12_Inactive_Lowest_Value_Customer'
            END AS Segment, 
      FROM VIEW a""",'acm-bi.BI_TARGET_LIST.BI_MODEL_VALUE_SEGMENTATION_MASTER')


# ## 7-ELEVEN: TMN WALLET

# In[4]:


gbq.gbq_insert("""WITH TRANSACTION_DATA AS (SELECT a.TMNID, TRANS_DATETIME, AMOUNT, EXTRACT(month FROM TRANS_DATETIME) AS month,a.SERVICE_LEVEL1,CHANNEL_APP,
                                  RANK() OVER (PARTITION BY a.TMNID ORDER BY TRANS_DATETIME DESC ) AS rank   
                            FROM `acm-prod.datamart_std.DM_V1_MOVEMENT_TRANSACTION` a
                            JOIN `acm-prod.datamart.DM_V1_TMNID_MASTER` b
                                  ON a.tmnid = b.TMNID
                                  AND b.DELETED_DATE IS NULL
                            WHERE DATE(TRANS_DATETIME) BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL -180 day) AND CURRENT_DATE
                                  AND a.SERVICE_LEVEL1  = '7-ELEVEN' and a.CHANNEL_APP  = 'TMN WALLET'
                                  AND PMAU_FLAG = 1
                           ),
    RECENCY_DATA AS (SELECT tmnid,
                            DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) AS Recency,
                            PERCENT_RANK() OVER(ORDER BY DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) DESC) AS Percent_Rank_Recency
                        FROM TRANSACTION_DATA
                        WHERE rank = 1),
    FREQUENCY_DATA AS (SELECT tmnid,
                              COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month) AS Frequency,
                              PERCENT_RANK() OVER(ORDER BY COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month)) AS Percent_Rank_Frequency
                         FROM TRANSACTION_DATA
                         GROUP BY 1),
    MONETARY_DATA AS (SELECT tmnid,
                            SUM(AMOUNT)/ COUNT(DISTINCT month) AS Monetary,
                            PERCENT_RANK() OVER(ORDER BY SUM(AMOUNT)/ COUNT(DISTINCT month) ) AS Percent_Rank_Monetary
                          FROM TRANSACTION_DATA 
                          GROUP BY 1 ),
    VIEW AS (SELECT DISTINCT SERVICE_LEVEL1,CHANNEL_APP as SERVICE_LEVEL2 , a.tmnid, 
                    b.Recency, Percent_Rank_REcency,
                    CASE  WHEN b.Recency < 8 THEN 4
                          WHEN b.Recency BETWEEN 8 AND 20 THEN 3
                          WHEN b.Recency BETWEEN 20 AND 50 THEN 2
                          WHEN b.Recency >50 THEN 1
                    END AS R_SCORE,
                    c.Frequency, Percent_Rank_Frequency,
                    CASE  WHEN Frequency BETWEEN 0 AND 1 THEN 1
                          WHEN Frequency < 6 THEN 2
                          WHEN Frequency < 15 THEN 3
                          WHEN Frequency >= 15 THEN 4
                    END AS F_SCORE,
                    d.Monetary, Percent_Rank_Monetary,
                    CASE  WHEN Monetary < 200 THEN 1
                          WHEN Monetary BETWEEN 200 AND 700 THEN 2
                          WHEN Monetary BETWEEN 700 AND 1500 THEN 3
                          WHEN Monetary > 1500 THEN 4
                    END AS M_SCORE
                FROM TRANSACTION_DATA a
                LEFT JOIN RECENCY_DATA b
                      ON a.TMNID = b.TMNID
                LEFT JOIN Frequency_DATA c
                      ON a.TMNID = c.TMNID
                LEFT JOIN MONETARY_DATA d
                      ON a.TMNID = d.TMNID )
        SELECT DISTINCT CURRENT_DATE as MODEL_MONTH,'MONTHLY' as RUN_TYPE, a.*,
            CASE  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) = '444' THEN '1_Champion Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('314','324','334','344','414','424','434') THEN '2_Active_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('332', '333', '342', '343', '432', '433', '442', '443') THEN '3_Active_Medium_Value_Sticky_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('312','313','322','323','412','413','422','423') THEN '4_Active_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('311', '321', '331', '341', '411', '421', '431', '441') THEN '5_Active_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('214','224','234','244') THEN '6_Churn_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('212', '213', '222', '223', '232', '233', '242', '243') THEN '7_Churn_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('211','221','231','241') THEN '8_Churn_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('114', '124', '134', '144') THEN '9_Inactive_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('112','113','122','123','132','133','142','143') THEN '10_Inactive_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('121', '131', '141') THEN '11_Inactive_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('111') THEN '12_Inactive_Lowest_Value_Customer'
            END AS Segment, 
      FROM VIEW a""",'acm-bi.BI_TARGET_LIST.BI_MODEL_VALUE_SEGMENTATION_MASTER')


# ## VAS TOPUP: ALL CHANNELS 

# In[5]:


gbq.gbq_insert("""WITH TRANSACTION_DATA AS (SELECT a.TMNID, TRANS_DATETIME, AMOUNT, EXTRACT(month FROM TRANS_DATETIME) AS month,a.SERVICE_LEVEL1,channel_app,
                                  RANK() OVER (PARTITION BY a.TMNID ORDER BY TRANS_DATETIME DESC ) AS rank   
                            FROM `acm-prod.datamart_std.DM_V1_MOVEMENT_TRANSACTION` a
                            JOIN `acm-prod.datamart.DM_V1_TMNID_MASTER` b
                                  ON a.tmnid = b.TMNID
                                  AND b.DELETED_DATE IS NULL
                            WHERE DATE(TRANS_DATETIME) BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL -180 day) AND CURRENT_DATE
                                  AND a.SERVICE_LEVEL1  = 'VAS TOPUP'
                                  AND PMAU_FLAG = 1
                           ),
    RECENCY_DATA AS (SELECT tmnid,
                            DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) AS Recency,
                            PERCENT_RANK() OVER(ORDER BY DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) DESC) AS Percent_Rank_Recency
                        FROM TRANSACTION_DATA
                        WHERE rank = 1),
    FREQUENCY_DATA AS (SELECT tmnid,
                              COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month) AS Frequency,
                              PERCENT_RANK() OVER(ORDER BY COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month)) AS Percent_Rank_Frequency
                         FROM TRANSACTION_DATA
                         GROUP BY 1),
    MONETARY_DATA AS (SELECT tmnid,
                            SUM(AMOUNT)/ COUNT(DISTINCT month) AS Monetary,
                            PERCENT_RANK() OVER(ORDER BY SUM(AMOUNT)/ COUNT(DISTINCT month) ) AS Percent_Rank_Monetary
                          FROM TRANSACTION_DATA 
                          GROUP BY 1 ),
    VIEW AS (SELECT DISTINCT SERVICE_LEVEL1,'VAS TOPUP' as SERVICE_LEVEL2 , a.tmnid, 
                    b.Recency, Percent_Rank_REcency,
                    CASE  WHEN b.Recency < 8 THEN 4
                          WHEN b.Recency BETWEEN 8 AND 20 THEN 3
                          WHEN b.Recency BETWEEN 20 AND 50 THEN 2
                          WHEN b.Recency >50 THEN 1
                    END AS R_SCORE,
                    c.Frequency, Percent_Rank_Frequency,
                    CASE  WHEN Frequency BETWEEN 0 AND 1 THEN 1
                          WHEN Frequency < 6 THEN 2
                          WHEN Frequency < 15 THEN 3
                          WHEN Frequency >= 15 THEN 4
                    END AS F_SCORE,
                    d.Monetary, Percent_Rank_Monetary,
                    CASE  WHEN Monetary < 80 THEN 1
                          WHEN Monetary BETWEEN 80 AND 300 THEN 2
                          WHEN Monetary BETWEEN 300 AND 500 THEN 3
                          WHEN Monetary > 500 THEN 4
                    END AS M_SCORE
                FROM TRANSACTION_DATA a
                LEFT JOIN RECENCY_DATA b
                      ON a.TMNID = b.TMNID
                LEFT JOIN Frequency_DATA c
                      ON a.TMNID = c.TMNID
                LEFT JOIN MONETARY_DATA d
                      ON a.TMNID = d.TMNID )
        SELECT DISTINCT CURRENT_DATE as MODEL_MONTH,'MONTHLY' as RUN_TYPE, a.*,
            CASE  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) = '444' THEN '1_Champion Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('314','324','334','344','414','424','434') THEN '2_Active_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('332', '333', '342', '343', '432', '433', '442', '443') THEN '3_Active_Medium_Value_Sticky_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('312','313','322','323','412','413','422','423') THEN '4_Active_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('311', '321', '331', '341', '411', '421', '431', '441') THEN '5_Active_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('214','224','234','244') THEN '6_Churn_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('212', '213', '222', '223', '232', '233', '242', '243') THEN '7_Churn_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('211','221','231','241') THEN '8_Churn_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('114', '124', '134', '144') THEN '9_Inactive_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('112','113','122','123','132','133','142','143') THEN '10_Inactive_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('121', '131', '141') THEN '11_Inactive_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('111') THEN '12_Inactive_Lowest_Value_Customer'
            END AS Segment, 
      FROM VIEW a""",'acm-bi.BI_TARGET_LIST.BI_MODEL_VALUE_SEGMENTATION_MASTER')


# ## VAS TOPUP: E-CAT

# In[6]:


gbq.gbq_insert("""WITH TRANSACTION_DATA AS (SELECT a.TMNID, TRANS_DATETIME, AMOUNT, EXTRACT(month FROM TRANS_DATETIME) AS month,a.SERVICE_LEVEL1,channel_app,
                                  RANK() OVER (PARTITION BY a.TMNID ORDER BY TRANS_DATETIME DESC ) AS rank   
                            FROM `acm-prod.datamart_std.DM_V1_MOVEMENT_TRANSACTION` a
                            JOIN `acm-prod.datamart.DM_V1_TMNID_MASTER` b
                                  ON a.tmnid = b.TMNID
                                  AND b.DELETED_DATE IS NULL
                            WHERE DATE(TRANS_DATETIME) BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL -180 day) AND CURRENT_DATE
                                  AND a.SERVICE_LEVEL1  = 'VAS TOPUP' and a.CHANNEL_APP  = 'HLIFE_WEB_TOPUP_VAS'
                                  AND PMAU_FLAG = 1
                           ),
    RECENCY_DATA AS (SELECT tmnid,
                            DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) AS Recency,
                            PERCENT_RANK() OVER(ORDER BY DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) DESC) AS Percent_Rank_Recency
                        FROM TRANSACTION_DATA
                        WHERE rank = 1),
    FREQUENCY_DATA AS (SELECT tmnid,
                              COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month) AS Frequency,
                              PERCENT_RANK() OVER(ORDER BY COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month)) AS Percent_Rank_Frequency
                         FROM TRANSACTION_DATA
                         GROUP BY 1),
    MONETARY_DATA AS (SELECT tmnid,
                            SUM(AMOUNT)/ COUNT(DISTINCT month) AS Monetary,
                            PERCENT_RANK() OVER(ORDER BY SUM(AMOUNT)/ COUNT(DISTINCT month) ) AS Percent_Rank_Monetary
                          FROM TRANSACTION_DATA 
                          GROUP BY 1 ),
    VIEW AS (SELECT DISTINCT SERVICE_LEVEL1,'E-CAT' as SERVICE_LEVEL2 , a.tmnid, 
                    b.Recency, Percent_Rank_REcency,
                    CASE  WHEN b.Recency < 8 THEN 4
                          WHEN b.Recency BETWEEN 8 AND 20 THEN 3
                          WHEN b.Recency BETWEEN 20 AND 50 THEN 2
                          WHEN b.Recency >50 THEN 1
                    END AS R_SCORE,
                    c.Frequency, Percent_Rank_Frequency,
                    CASE  WHEN Frequency BETWEEN 0 AND 1 THEN 1
                          WHEN Frequency < 6 THEN 2
                          WHEN Frequency < 15 THEN 3
                          WHEN Frequency >= 15 THEN 4
                    END AS F_SCORE,
                    d.Monetary, Percent_Rank_Monetary,
                    CASE  WHEN Monetary < 80 THEN 1
                          WHEN Monetary BETWEEN 80 AND 300 THEN 2
                          WHEN Monetary BETWEEN 300 AND 500 THEN 3
                          WHEN Monetary > 500 THEN 4
                    END AS M_SCORE
                FROM TRANSACTION_DATA a
                LEFT JOIN RECENCY_DATA b
                      ON a.TMNID = b.TMNID
                LEFT JOIN Frequency_DATA c
                      ON a.TMNID = c.TMNID
                LEFT JOIN MONETARY_DATA d
                      ON a.TMNID = d.TMNID )
        SELECT DISTINCT CURRENT_DATE as MODEL_MONTH,'MONTHLY' as RUN_TYPE, a.*,
            CASE  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) = '444' THEN '1_Champion Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('314','324','334','344','414','424','434') THEN '2_Active_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('332', '333', '342', '343', '432', '433', '442', '443') THEN '3_Active_Medium_Value_Sticky_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('312','313','322','323','412','413','422','423') THEN '4_Active_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('311', '321', '331', '341', '411', '421', '431', '441') THEN '5_Active_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('214','224','234','244') THEN '6_Churn_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('212', '213', '222', '223', '232', '233', '242', '243') THEN '7_Churn_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('211','221','231','241') THEN '8_Churn_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('114', '124', '134', '144') THEN '9_Inactive_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('112','113','122','123','132','133','142','143') THEN '10_Inactive_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('121', '131', '141') THEN '11_Inactive_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('111') THEN '12_Inactive_Lowest_Value_Customer'
            END AS Segment, 
      FROM VIEW a""",'acm-bi.BI_TARGET_LIST.BI_MODEL_VALUE_SEGMENTATION_MASTER')


# ## VAS TOPUP: TMN WALLET

# In[7]:


gbq.gbq_insert("""WITH TRANSACTION_DATA AS (SELECT a.TMNID, TRANS_DATETIME, AMOUNT, EXTRACT(month FROM TRANS_DATETIME) AS month,a.SERVICE_LEVEL1,channel_app,
                                  RANK() OVER (PARTITION BY a.TMNID ORDER BY TRANS_DATETIME DESC ) AS rank   
                            FROM `acm-prod.datamart_std.DM_V1_MOVEMENT_TRANSACTION` a
                            JOIN `acm-prod.datamart.DM_V1_TMNID_MASTER` b
                                  ON a.tmnid = b.TMNID
                                  AND b.DELETED_DATE IS NULL
                            WHERE DATE(TRANS_DATETIME) BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL -180 day) AND CURRENT_DATE
                                  AND a.SERVICE_LEVEL1  = 'VAS TOPUP' and a.CHANNEL_APP  = 'TMN WALLET'
                                  AND PMAU_FLAG = 1
                           ),
    RECENCY_DATA AS (SELECT tmnid,
                            DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) AS Recency,
                            PERCENT_RANK() OVER(ORDER BY DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) DESC) AS Percent_Rank_Recency
                        FROM TRANSACTION_DATA
                        WHERE rank = 1),
    FREQUENCY_DATA AS (SELECT tmnid,
                              COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month) AS Frequency,
                              PERCENT_RANK() OVER(ORDER BY COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month)) AS Percent_Rank_Frequency
                         FROM TRANSACTION_DATA
                         GROUP BY 1),
    MONETARY_DATA AS (SELECT tmnid,
                            SUM(AMOUNT)/ COUNT(DISTINCT month) AS Monetary,
                            PERCENT_RANK() OVER(ORDER BY SUM(AMOUNT)/ COUNT(DISTINCT month) ) AS Percent_Rank_Monetary
                          FROM TRANSACTION_DATA 
                          GROUP BY 1 ),
    VIEW AS (SELECT DISTINCT SERVICE_LEVEL1,'TMN WALLET' as SERVICE_LEVEL2 , a.tmnid, 
                    b.Recency, Percent_Rank_REcency,
                    CASE  WHEN b.Recency < 8 THEN 4
                          WHEN b.Recency BETWEEN 8 AND 20 THEN 3
                          WHEN b.Recency BETWEEN 20 AND 50 THEN 2
                          WHEN b.Recency >50 THEN 1
                    END AS R_SCORE,
                    c.Frequency, Percent_Rank_Frequency,
                    CASE  WHEN Frequency BETWEEN 0 AND 1 THEN 1
                          WHEN Frequency < 6 THEN 2
                          WHEN Frequency < 15 THEN 3
                          WHEN Frequency >= 15 THEN 4
                    END AS F_SCORE,
                    d.Monetary, Percent_Rank_Monetary,
                    CASE  WHEN Monetary < 80 THEN 1
                          WHEN Monetary BETWEEN 80 AND 300 THEN 2
                          WHEN Monetary BETWEEN 300 AND 500 THEN 3
                          WHEN Monetary > 500 THEN 4
                    END AS M_SCORE
                FROM TRANSACTION_DATA a
                LEFT JOIN RECENCY_DATA b
                      ON a.TMNID = b.TMNID
                LEFT JOIN Frequency_DATA c
                      ON a.TMNID = c.TMNID
                LEFT JOIN MONETARY_DATA d
                      ON a.TMNID = d.TMNID )
        SELECT DISTINCT CURRENT_DATE as MODEL_MONTH,'MONTHLY' as RUN_TYPE, a.*,
            CASE  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) = '444' THEN '1_Champion Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('314','324','334','344','414','424','434') THEN '2_Active_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('332', '333', '342', '343', '432', '433', '442', '443') THEN '3_Active_Medium_Value_Sticky_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('312','313','322','323','412','413','422','423') THEN '4_Active_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('311', '321', '331', '341', '411', '421', '431', '441') THEN '5_Active_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('214','224','234','244') THEN '6_Churn_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('212', '213', '222', '223', '232', '233', '242', '243') THEN '7_Churn_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('211','221','231','241') THEN '8_Churn_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('114', '124', '134', '144') THEN '9_Inactive_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('112','113','122','123','132','133','142','143') THEN '10_Inactive_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('121', '131', '141') THEN '11_Inactive_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('111') THEN '12_Inactive_Lowest_Value_Customer'
            END AS Segment, 
      FROM VIEW a""",'acm-bi.BI_TARGET_LIST.BI_MODEL_VALUE_SEGMENTATION_MASTER')


# ## BILL PAYMENT: TRUE

# In[8]:


gbq.gbq_insert("""WITH TRANSACTION_DATA AS (SELECT a.TMNID, TRANS_DATETIME, AMOUNT, EXTRACT(month FROM TRANS_DATETIME) AS month,a.SERVICE_LEVEL1,
                                  RANK() OVER (PARTITION BY a.TMNID ORDER BY TRANS_DATETIME DESC ) AS rank   
                            FROM `acm-prod.datamart_std.DM_V1_MOVEMENT_TRANSACTION` a
                            JOIN `acm-prod.datamart.DM_V1_TMNID_MASTER` b
                                  ON a.tmnid = b.TMNID
                                  AND b.DELETED_DATE IS NULL
                            WHERE DATE(TRANS_DATETIME) BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL -180 day) AND CURRENT_DATE
                                  AND a.SERVICE_LEVEL1  = 'BILL PAYMENT' and SERVICE_LEVEL2  = 'TRUE GROUP'
                                  AND PMAU_FLAG = 1
                           ),
    RECENCY_DATA AS (SELECT tmnid,
                            DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) AS Recency,
                            PERCENT_RANK() OVER(ORDER BY DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) DESC) AS Percent_Rank_Recency
                        FROM TRANSACTION_DATA
                        WHERE rank = 1),
    FREQUENCY_DATA AS (SELECT tmnid,
                              COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month) AS Frequency,
                              PERCENT_RANK() OVER(ORDER BY COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month)) AS Percent_Rank_Frequency
                         FROM TRANSACTION_DATA
                         GROUP BY 1),
    MONETARY_DATA AS (SELECT tmnid,
                            SUM(AMOUNT)/ COUNT(DISTINCT month) AS Monetary,
                            PERCENT_RANK() OVER(ORDER BY SUM(AMOUNT)/ COUNT(DISTINCT month) ) AS Percent_Rank_Monetary
                          FROM TRANSACTION_DATA 
                          GROUP BY 1 ),
    VIEW AS (SELECT DISTINCT SERVICE_LEVEL1,'TRUE GROUP' as SERVICE_LEVEL2 , a.tmnid, 
                    b.Recency, Percent_Rank_REcency,
                    CASE  WHEN b.Recency < 8 THEN 4
                          WHEN b.Recency BETWEEN 8 AND 20 THEN 3
                          WHEN b.Recency BETWEEN 20 AND 50 THEN 2
                          WHEN b.Recency >50 THEN 1
                    END AS R_SCORE,
                    c.Frequency, Percent_Rank_Frequency,
                    CASE  WHEN Frequency BETWEEN 0 AND 1 THEN 1
                          WHEN Frequency < 2 THEN 2
                          WHEN Frequency < 3 THEN 3
                          WHEN Frequency >= 3 THEN 4
                    END AS F_SCORE,
                    d.Monetary, Percent_Rank_Monetary,
                    CASE  WHEN Monetary < 600 THEN 1
                          WHEN Monetary BETWEEN 600 AND 1100 THEN 2
                          WHEN Monetary BETWEEN 1100 AND 1700 THEN 3
                          WHEN Monetary > 1700 THEN 4
                    END AS M_SCORE
                FROM TRANSACTION_DATA a
                LEFT JOIN RECENCY_DATA b
                      ON a.TMNID = b.TMNID
                LEFT JOIN Frequency_DATA c
                      ON a.TMNID = c.TMNID
                LEFT JOIN MONETARY_DATA d
                      ON a.TMNID = d.TMNID )
        SELECT DISTINCT CURRENT_DATE as MODEL_MONTH,'MONTHLY' as RUN_TYPE, a.*,
            CASE  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) = '444' THEN '1_Champion Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('314','324','334','344','414','424','434') THEN '2_Active_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('332', '333', '342', '343', '432', '433', '442', '443') THEN '3_Active_Medium_Value_Sticky_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('312','313','322','323','412','413','422','423') THEN '4_Active_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('311', '321', '331', '341', '411', '421', '431', '441') THEN '5_Active_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('214','224','234','244') THEN '6_Churn_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('212', '213', '222', '223', '232', '233', '242', '243') THEN '7_Churn_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('211','221','231','241') THEN '8_Churn_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('114', '124', '134', '144') THEN '9_Inactive_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('112','113','122','123','132','133','142','143') THEN '10_Inactive_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('121', '131', '141') THEN '11_Inactive_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('111') THEN '12_Inactive_Lowest_Value_Customer'
            END AS Segment, 
      FROM VIEW a""",'acm-bi.BI_TARGET_LIST.BI_MODEL_VALUE_SEGMENTATION_MASTER')


# ## BILL PAYMENT: NON-TRUE

# In[9]:


gbq.gbq_insert("""WITH TRANSACTION_DATA AS (SELECT a.TMNID, TRANS_DATETIME, AMOUNT, EXTRACT(month FROM TRANS_DATETIME) AS month,a.SERVICE_LEVEL1,channel_app,
                                  RANK() OVER (PARTITION BY a.TMNID ORDER BY TRANS_DATETIME DESC ) AS rank   
                            FROM `acm-prod.datamart_std.DM_V1_MOVEMENT_TRANSACTION` a
                            JOIN `acm-prod.datamart.DM_V1_TMNID_MASTER` b
                                  ON a.tmnid = b.TMNID
                                  AND b.DELETED_DATE IS NULL
                            WHERE DATE(TRANS_DATETIME) BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL -180 day) AND CURRENT_DATE
                                  AND a.SERVICE_LEVEL1  = 'BILL PAYMENT' and a.CHANNEL_APP  = 'NON-TRUE GROUP'
                                  AND PMAU_FLAG = 1
                           ),
    RECENCY_DATA AS (SELECT tmnid,
                            DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) AS Recency,
                            PERCENT_RANK() OVER(ORDER BY DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) DESC) AS Percent_Rank_Recency
                        FROM TRANSACTION_DATA
                        WHERE rank = 1),
    FREQUENCY_DATA AS (SELECT tmnid,
                              COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month) AS Frequency,
                              PERCENT_RANK() OVER(ORDER BY COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month)) AS Percent_Rank_Frequency
                         FROM TRANSACTION_DATA
                         GROUP BY 1),
    MONETARY_DATA AS (SELECT tmnid,
                            SUM(AMOUNT)/ COUNT(DISTINCT month) AS Monetary,
                            PERCENT_RANK() OVER(ORDER BY SUM(AMOUNT)/ COUNT(DISTINCT month) ) AS Percent_Rank_Monetary
                          FROM TRANSACTION_DATA 
                          GROUP BY 1 ),
    VIEW AS (SELECT DISTINCT SERVICE_LEVEL1,'NON-TRUE GROUP' as SERVICE_LEVEL2 , a.tmnid, 
                    b.Recency, Percent_Rank_REcency,
                    CASE  WHEN b.Recency < 8 THEN 4
                          WHEN b.Recency BETWEEN 8 AND 20 THEN 3
                          WHEN b.Recency BETWEEN 20 AND 50 THEN 2
                          WHEN b.Recency >50 THEN 1
                    END AS R_SCORE,
                    c.Frequency, Percent_Rank_Frequency,
                    CASE  WHEN Frequency BETWEEN 0 AND 1 THEN 1
                          WHEN Frequency < 3 THEN 2
                          WHEN Frequency < 4 THEN 3
                          WHEN Frequency >= 4 THEN 4
                    END AS F_SCORE,
                    d.Monetary, Percent_Rank_Monetary,
                    CASE  WHEN Monetary < 900 THEN 1
                          WHEN Monetary BETWEEN 900 AND 3000 THEN 2
                          WHEN Monetary BETWEEN 3000 AND 7600 THEN 3
                          WHEN Monetary > 7600 THEN 4
                    END AS M_SCORE
                FROM TRANSACTION_DATA a
                LEFT JOIN RECENCY_DATA b
                      ON a.TMNID = b.TMNID
                LEFT JOIN Frequency_DATA c
                      ON a.TMNID = c.TMNID
                LEFT JOIN MONETARY_DATA d
                      ON a.TMNID = d.TMNID )
        SELECT DISTINCT CURRENT_DATE as MODEL_MONTH,'MONTHLY' as RUN_TYPE, a.*,
            CASE  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) = '444' THEN '1_Champion Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('314','324','334','344','414','424','434') THEN '2_Active_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('332', '333', '342', '343', '432', '433', '442', '443') THEN '3_Active_Medium_Value_Sticky_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('312','313','322','323','412','413','422','423') THEN '4_Active_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('311', '321', '331', '341', '411', '421', '431', '441') THEN '5_Active_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('214','224','234','244') THEN '6_Churn_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('212', '213', '222', '223', '232', '233', '242', '243') THEN '7_Churn_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('211','221','231','241') THEN '8_Churn_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('114', '124', '134', '144') THEN '9_Inactive_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('112','113','122','123','132','133','142','143') THEN '10_Inactive_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('121', '131', '141') THEN '11_Inactive_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('111') THEN '12_Inactive_Lowest_Value_Customer'
            END AS Segment, 
      FROM VIEW a""",'acm-bi.BI_TARGET_LIST.BI_MODEL_VALUE_SEGMENTATION_MASTER')


# ## P2P (SENDER)

# In[10]:


gbq.gbq_insert("""WITH TRANSACTION_DATA AS (SELECT a.TMNID, TRANS_DATETIME, AMOUNT, EXTRACT(month FROM TRANS_DATETIME) AS month,a.SERVICE_LEVEL1,
                                  RANK() OVER (PARTITION BY a.TMNID ORDER BY TRANS_DATETIME DESC ) AS rank   
                            FROM `acm-prod.datamart_std.DM_V1_MOVEMENT_TRANSACTION` a
                            JOIN `acm-prod.datamart.DM_V1_TMNID_MASTER` b
                                  ON a.tmnid = b.TMNID
                                  AND b.DELETED_DATE IS NULL
                            WHERE DATE(TRANS_DATETIME) BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL -180 day) AND CURRENT_DATE
                                  AND a.SERVICE_LEVEL1  = 'P2P (SENDER)' 
                                  AND PMAU_FLAG = 1
                           ),
    RECENCY_DATA AS (SELECT tmnid,
                            DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) AS Recency,
                            PERCENT_RANK() OVER(ORDER BY DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) DESC) AS Percent_Rank_Recency
                        FROM TRANSACTION_DATA
                        WHERE rank = 1),
    FREQUENCY_DATA AS (SELECT tmnid,
                              COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month) AS Frequency,
                              PERCENT_RANK() OVER(ORDER BY COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month)) AS Percent_Rank_Frequency
                         FROM TRANSACTION_DATA
                         GROUP BY 1),
    MONETARY_DATA AS (SELECT tmnid,
                            SUM(AMOUNT)/ COUNT(DISTINCT month) AS Monetary,
                            PERCENT_RANK() OVER(ORDER BY SUM(AMOUNT)/ COUNT(DISTINCT month) ) AS Percent_Rank_Monetary
                          FROM TRANSACTION_DATA 
                          GROUP BY 1 ),
    VIEW AS (SELECT DISTINCT SERVICE_LEVEL1,'P2P (SENDER)' as SERVICE_LEVEL2 , a.tmnid, 
                    b.Recency, Percent_Rank_REcency,
                    CASE  WHEN b.Recency < 8 THEN 4
                          WHEN b.Recency BETWEEN 8 AND 20 THEN 3
                          WHEN b.Recency BETWEEN 20 AND 50 THEN 2
                          WHEN b.Recency >50 THEN 1
                    END AS R_SCORE,
                    c.Frequency, Percent_Rank_Frequency,
                    CASE  WHEN Frequency BETWEEN 0 AND 1 THEN 1
                          WHEN Frequency < 5 THEN 2
                          WHEN Frequency < 12 THEN 3
                          WHEN Frequency >= 12 THEN 4
                    END AS F_SCORE,
                    d.Monetary, Percent_Rank_Monetary,
                    CASE  WHEN Monetary < 60 THEN 1
                          WHEN Monetary BETWEEN 60 AND 400 THEN 2
                          WHEN Monetary BETWEEN 400 AND 1300 THEN 3
                          WHEN Monetary > 1300 THEN 4
                    END AS M_SCORE
                FROM TRANSACTION_DATA a
                LEFT JOIN RECENCY_DATA b
                      ON a.TMNID = b.TMNID
                LEFT JOIN Frequency_DATA c
                      ON a.TMNID = c.TMNID
                LEFT JOIN MONETARY_DATA d
                      ON a.TMNID = d.TMNID )
        SELECT DISTINCT CURRENT_DATE as MODEL_MONTH,'MONTHLY' as RUN_TYPE, a.*,
            CASE  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) = '444' THEN '1_Champion Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('314','324','334','344','414','424','434') THEN '2_Active_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('332', '333', '342', '343', '432', '433', '442', '443') THEN '3_Active_Medium_Value_Sticky_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('312','313','322','323','412','413','422','423') THEN '4_Active_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('311', '321', '331', '341', '411', '421', '431', '441') THEN '5_Active_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('214','224','234','244') THEN '6_Churn_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('212', '213', '222', '223', '232', '233', '242', '243') THEN '7_Churn_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('211','221','231','241') THEN '8_Churn_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('114', '124', '134', '144') THEN '9_Inactive_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('112','113','122','123','132','133','142','143') THEN '10_Inactive_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('121', '131', '141') THEN '11_Inactive_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('111') THEN '12_Inactive_Lowest_Value_Customer'
            END AS Segment, 
      FROM VIEW a""",'acm-bi.BI_TARGET_LIST.BI_MODEL_VALUE_SEGMENTATION_MASTER')


# ## MOBILE TOPUP

# In[11]:


gbq.gbq_insert("""WITH TRANSACTION_DATA AS (SELECT a.TMNID, TRANS_DATETIME, AMOUNT, EXTRACT(month FROM TRANS_DATETIME) AS month,a.SERVICE_LEVEL1,
                                  RANK() OVER (PARTITION BY a.TMNID ORDER BY TRANS_DATETIME DESC ) AS rank   
                            FROM `acm-prod.datamart_std.DM_V1_MOVEMENT_TRANSACTION` a
                            JOIN `acm-prod.datamart.DM_V1_TMNID_MASTER` b
                                  ON a.tmnid = b.TMNID
                                  AND b.DELETED_DATE IS NULL
                            WHERE DATE(TRANS_DATETIME) BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL -180 day) AND CURRENT_DATE
                                  AND a.SERVICE_LEVEL1  = 'MOBILE TOPUP'
                                  AND PMAU_FLAG = 1
                           ),
    RECENCY_DATA AS (SELECT tmnid,
                            DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) AS Recency,
                            PERCENT_RANK() OVER(ORDER BY DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) DESC) AS Percent_Rank_Recency
                        FROM TRANSACTION_DATA
                        WHERE rank = 1),
    FREQUENCY_DATA AS (SELECT tmnid,
                              COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month) AS Frequency,
                              PERCENT_RANK() OVER(ORDER BY COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month)) AS Percent_Rank_Frequency
                         FROM TRANSACTION_DATA
                         GROUP BY 1),
    MONETARY_DATA AS (SELECT tmnid,
                            SUM(AMOUNT)/ COUNT(DISTINCT month) AS Monetary,
                            PERCENT_RANK() OVER(ORDER BY SUM(AMOUNT)/ COUNT(DISTINCT month) ) AS Percent_Rank_Monetary
                          FROM TRANSACTION_DATA 
                          GROUP BY 1 ),
    VIEW AS (SELECT DISTINCT SERVICE_LEVEL1,'MOBILE TOPUP'  as SERVICE_LEVEL2 , a.tmnid, 
                    b.Recency, Percent_Rank_REcency,
                    CASE  WHEN b.Recency < 8 THEN 4
                          WHEN b.Recency BETWEEN 8 AND 20 THEN 3
                          WHEN b.Recency BETWEEN 20 AND 50 THEN 2
                          WHEN b.Recency >50 THEN 1
                    END AS R_SCORE,
                    c.Frequency, Percent_Rank_Frequency,
                    CASE  WHEN Frequency BETWEEN 0 AND 1 THEN 1
                          WHEN Frequency < 3 THEN 2
                          WHEN Frequency < 4 THEN 3
                          WHEN Frequency >= 4 THEN 4
                    END AS F_SCORE,
                    d.Monetary, Percent_Rank_Monetary,
                    CASE  WHEN Monetary < 30 THEN 1
                          WHEN Monetary BETWEEN 30 AND 100 THEN 2
                          WHEN Monetary BETWEEN 100 AND 200 THEN 3
                          WHEN Monetary > 200 THEN 4
                    END AS M_SCORE
                FROM TRANSACTION_DATA a
                LEFT JOIN RECENCY_DATA b
                      ON a.TMNID = b.TMNID
                LEFT JOIN Frequency_DATA c
                      ON a.TMNID = c.TMNID
                LEFT JOIN MONETARY_DATA d
                      ON a.TMNID = d.TMNID )
        SELECT DISTINCT CURRENT_DATE as MODEL_MONTH,'MONTHLY' as RUN_TYPE, a.*,
            CASE  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) = '444' THEN '1_Champion Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('314','324','334','344','414','424','434') THEN '2_Active_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('332', '333', '342', '343', '432', '433', '442', '443') THEN '3_Active_Medium_Value_Sticky_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('312','313','322','323','412','413','422','423') THEN '4_Active_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('311', '321', '331', '341', '411', '421', '431', '441') THEN '5_Active_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('214','224','234','244') THEN '6_Churn_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('212', '213', '222', '223', '232', '233', '242', '243') THEN '7_Churn_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('211','221','231','241') THEN '8_Churn_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('114', '124', '134', '144') THEN '9_Inactive_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('112','113','122','123','132','133','142','143') THEN '10_Inactive_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('121', '131', '141') THEN '11_Inactive_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('111') THEN '12_Inactive_Lowest_Value_Customer'
            END AS Segment, 
      FROM VIEW a""",'acm-bi.BI_TARGET_LIST.BI_MODEL_VALUE_SEGMENTATION_MASTER')


# ## DONATION

# In[12]:


gbq.gbq_insert("""WITH TRANSACTION_DATA AS (SELECT a.TMNID, TRANS_DATETIME, AMOUNT, EXTRACT(month FROM TRANS_DATETIME) AS month,a.SERVICE_LEVEL1,
                                  RANK() OVER (PARTITION BY a.TMNID ORDER BY TRANS_DATETIME DESC ) AS rank   
                            FROM `acm-prod.datamart_std.DM_V1_MOVEMENT_TRANSACTION` a
                            JOIN `acm-prod.datamart.DM_V1_TMNID_MASTER` b
                                  ON a.tmnid = b.TMNID
                                  AND b.DELETED_DATE IS NULL
                            WHERE DATE(TRANS_DATETIME) BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL -180 day) AND CURRENT_DATE
                                  AND a.SERVICE_LEVEL1  = 'DONATION'
                                  AND PMAU_FLAG = 1
                           ),
    RECENCY_DATA AS (SELECT tmnid,
                            DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) AS Recency,
                            PERCENT_RANK() OVER(ORDER BY DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) DESC) AS Percent_Rank_Recency
                        FROM TRANSACTION_DATA
                        WHERE rank = 1),
    FREQUENCY_DATA AS (SELECT tmnid,
                              COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month) AS Frequency,
                              PERCENT_RANK() OVER(ORDER BY COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month)) AS Percent_Rank_Frequency
                         FROM TRANSACTION_DATA
                         GROUP BY 1),
    MONETARY_DATA AS (SELECT tmnid,
                            SUM(AMOUNT)/ COUNT(DISTINCT month) AS Monetary,
                            PERCENT_RANK() OVER(ORDER BY SUM(AMOUNT)/ COUNT(DISTINCT month) ) AS Percent_Rank_Monetary
                          FROM TRANSACTION_DATA 
                          GROUP BY 1 ),
    VIEW AS (SELECT DISTINCT SERVICE_LEVEL1,'DONATION' as SERVICE_LEVEL2 , a.tmnid, 
                    b.Recency, Percent_Rank_REcency,
                    CASE  WHEN b.Recency < 8 THEN 4
                          WHEN b.Recency BETWEEN 8 AND 20 THEN 3
                          WHEN b.Recency BETWEEN 20 AND 50 THEN 2
                          WHEN b.Recency >50 THEN 1
                    END AS R_SCORE,
                    c.Frequency, Percent_Rank_Frequency,
                    CASE  WHEN Frequency BETWEEN 0 AND 1 THEN 1
                          WHEN Frequency < 3 THEN 2
                          WHEN Frequency < 4 THEN 3
                          WHEN Frequency >= 4 THEN 4
                    END AS F_SCORE,
                    d.Monetary, Percent_Rank_Monetary,
                    CASE  WHEN Monetary < 6 THEN 1
                          WHEN Monetary BETWEEN 6 AND 13 THEN 2
                          WHEN Monetary BETWEEN 13 AND 60 THEN 3
                          WHEN Monetary > 60 THEN 4
                    END AS M_SCORE
                FROM TRANSACTION_DATA a
                LEFT JOIN RECENCY_DATA b
                      ON a.TMNID = b.TMNID
                LEFT JOIN Frequency_DATA c
                      ON a.TMNID = c.TMNID
                LEFT JOIN MONETARY_DATA d
                      ON a.TMNID = d.TMNID )
        SELECT DISTINCT CURRENT_DATE as MODEL_MONTH,'MONTHLY' as RUN_TYPE, a.*,
            CASE  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) = '444' THEN '1_Champion Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('314','324','334','344','414','424','434') THEN '2_Active_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('332', '333', '342', '343', '432', '433', '442', '443') THEN '3_Active_Medium_Value_Sticky_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('312','313','322','323','412','413','422','423') THEN '4_Active_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('311', '321', '331', '341', '411', '421', '431', '441') THEN '5_Active_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('214','224','234','244') THEN '6_Churn_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('212', '213', '222', '223', '232', '233', '242', '243') THEN '7_Churn_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('211','221','231','241') THEN '8_Churn_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('114', '124', '134', '144') THEN '9_Inactive_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('112','113','122','123','132','133','142','143') THEN '10_Inactive_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('121', '131', '141') THEN '11_Inactive_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('111') THEN '12_Inactive_Lowest_Value_Customer'
            END AS Segment, 
      FROM VIEW a""",'acm-bi.BI_TARGET_LIST.BI_MODEL_VALUE_SEGMENTATION_MASTER')


# ## ONLINE PAYMENT: LAZADA

# In[13]:


gbq.gbq_insert("""WITH TRANSACTION_DATA AS (SELECT a.TMNID, TRANS_DATETIME, AMOUNT, EXTRACT(month FROM TRANS_DATETIME) AS month,a.SERVICE_LEVEL1,
                                  RANK() OVER (PARTITION BY a.TMNID ORDER BY TRANS_DATETIME DESC ) AS rank   
                            FROM `acm-prod.datamart_std.DM_V1_MOVEMENT_TRANSACTION` a
                            JOIN `acm-prod.datamart.DM_V1_TMNID_MASTER` b
                                  ON a.tmnid = b.TMNID
                                  AND b.DELETED_DATE IS NULL
                            WHERE DATE(TRANS_DATETIME) BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL -180 day) AND CURRENT_DATE
                                  AND a.SERVICE_LEVEL1  = 'ONLINE PAYMENT' and a.SERVICE_LEVEL2  = 'LAZADA'
                                  AND PMAU_FLAG = 1
                           ),
    RECENCY_DATA AS (SELECT tmnid,
                            DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) AS Recency,
                            PERCENT_RANK() OVER(ORDER BY DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) DESC) AS Percent_Rank_Recency
                        FROM TRANSACTION_DATA
                        WHERE rank = 1),
    FREQUENCY_DATA AS (SELECT tmnid,
                              COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month) AS Frequency,
                              PERCENT_RANK() OVER(ORDER BY COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month)) AS Percent_Rank_Frequency
                         FROM TRANSACTION_DATA
                         GROUP BY 1),
    MONETARY_DATA AS (SELECT tmnid,
                            SUM(AMOUNT)/ COUNT(DISTINCT month) AS Monetary,
                            PERCENT_RANK() OVER(ORDER BY SUM(AMOUNT)/ COUNT(DISTINCT month) ) AS Percent_Rank_Monetary
                          FROM TRANSACTION_DATA 
                          GROUP BY 1 ),
    VIEW AS (SELECT DISTINCT SERVICE_LEVEL1,'LAZADA'  as SERVICE_LEVEL2 , a.tmnid, 
                    b.Recency, Percent_Rank_REcency,
                    CASE  WHEN b.Recency < 8 THEN 4
                          WHEN b.Recency BETWEEN 8 AND 20 THEN 3
                          WHEN b.Recency BETWEEN 20 AND 50 THEN 2
                          WHEN b.Recency >50 THEN 1
                    END AS R_SCORE,
                    c.Frequency, Percent_Rank_Frequency,
                    CASE  WHEN Frequency BETWEEN 0 AND 1 THEN 1
                          WHEN Frequency < 4 THEN 2
                          WHEN Frequency < 7 THEN 3
                          WHEN Frequency >= 7 THEN 4
                    END AS F_SCORE,
                    d.Monetary, Percent_Rank_Monetary,
                    CASE  WHEN Monetary < 150 THEN 1
                          WHEN Monetary BETWEEN 150 AND 500 THEN 2
                          WHEN Monetary BETWEEN 500 AND 1200 THEN 3
                          WHEN Monetary > 1200 THEN 4
                    END AS M_SCORE
                FROM TRANSACTION_DATA a
                LEFT JOIN RECENCY_DATA b
                      ON a.TMNID = b.TMNID
                LEFT JOIN Frequency_DATA c
                      ON a.TMNID = c.TMNID
                LEFT JOIN MONETARY_DATA d
                      ON a.TMNID = d.TMNID )
        SELECT DISTINCT CURRENT_DATE as MODEL_MONTH,'MONTHLY' as RUN_TYPE, a.*,
            CASE  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) = '444' THEN '1_Champion Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('314','324','334','344','414','424','434') THEN '2_Active_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('332', '333', '342', '343', '432', '433', '442', '443') THEN '3_Active_Medium_Value_Sticky_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('312','313','322','323','412','413','422','423') THEN '4_Active_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('311', '321', '331', '341', '411', '421', '431', '441') THEN '5_Active_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('214','224','234','244') THEN '6_Churn_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('212', '213', '222', '223', '232', '233', '242', '243') THEN '7_Churn_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('211','221','231','241') THEN '8_Churn_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('114', '124', '134', '144') THEN '9_Inactive_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('112','113','122','123','132','133','142','143') THEN '10_Inactive_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('121', '131', '141') THEN '11_Inactive_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('111') THEN '12_Inactive_Lowest_Value_Customer'
            END AS Segment, 
      FROM VIEW a""",'acm-bi.BI_TARGET_LIST.BI_MODEL_VALUE_SEGMENTATION_MASTER')


# ## ONLINE PAYMENT: OTHERS

# In[14]:


gbq.gbq_insert("""WITH UNION_DATA AS (SELECT a.TMNID, TRANS_DATETIME, AMOUNT, EXTRACT(month FROM TRANS_DATETIME) AS month,a.SERVICE_LEVEL1
                            FROM `acm-prod.datamart_std.DM_V1_MOVEMENT_TRANSACTION` a
                            JOIN `acm-prod.datamart.DM_V1_TMNID_MASTER` b
                                  ON a.tmnid = b.TMNID
                                  AND b.DELETED_DATE IS NULL
                            WHERE DATE(TRANS_DATETIME) BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL -180 day) AND CURRENT_DATE
                                  AND DATE(TRANS_DATETIME) >= '2021-01-01'
                                  AND a.SERVICE_LEVEL1  = 'ONLINE PAYMENT' and a.SERVICE_LEVEL2  in ('ONLINE PAYMENT OTHERS')
                                  AND PMAU_FLAG = 1
UNION ALL
SELECT a.TMNID, TRANS_DATETIME, AMOUNT, EXTRACT(month FROM TRANS_DATETIME) AS month,a.SERVICE_LEVEL1
                            FROM `acm-prod.datamart_bi.DM_V1_MOVEMENT_TRANSACTION_2020` a
                            JOIN `acm-prod.datamart.DM_V1_TMNID_MASTER` b
                                  ON a.tmnid = b.TMNID
                                  AND b.DELETED_DATE IS NULL
                            WHERE DATE(TRANS_DATETIME) BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL -180 day) AND CURRENT_DATE
                                  AND DATE(TRANS_DATETIME) >= '2020-01-01'
                                  AND a.SERVICE_LEVEL1  = 'ONLINE PAYMENT' and a.SERVICE_LEVEL2  in ('ONLINE PAYMENT OTHERS')
                                  AND PMAU_FLAG = 1
                                  
                                  ),
                                  
    TRANSACTION_DATA AS  (select *,RANK() OVER (PARTITION BY TMNID ORDER BY TRANS_DATETIME DESC ) AS rank
                                  from UNION_DATA
                               ),
    RECENCY_DATA AS (SELECT tmnid,
                            DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) AS Recency,
                            PERCENT_RANK() OVER(ORDER BY DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) DESC) AS Percent_Rank_Recency
                        FROM TRANSACTION_DATA
                        WHERE rank = 1),
    FREQUENCY_DATA AS (SELECT tmnid,
                              COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month) AS Frequency,
                              PERCENT_RANK() OVER(ORDER BY COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month)) AS Percent_Rank_Frequency
                         FROM TRANSACTION_DATA
                         GROUP BY 1),
    MONETARY_DATA AS (SELECT tmnid,
                            SUM(AMOUNT)/ COUNT(DISTINCT month) AS Monetary,
                            PERCENT_RANK() OVER(ORDER BY SUM(AMOUNT)/ COUNT(DISTINCT month) ) AS Percent_Rank_Monetary
                          FROM TRANSACTION_DATA 
                          GROUP BY 1 ),
    VIEW AS (SELECT DISTINCT SERVICE_LEVEL1,'ONLINE PAYMENT OTHERS' as SERVICE_LEVEL2 , a.tmnid, 
                    b.Recency, Percent_Rank_REcency,
                    CASE  WHEN b.Recency < 8 THEN 4
                          WHEN b.Recency BETWEEN 8 AND 20 THEN 3
                          WHEN b.Recency BETWEEN 20 AND 50 THEN 2
                          WHEN b.Recency >50 THEN 1
                    END AS R_SCORE,
                    c.Frequency, Percent_Rank_Frequency,
                    CASE  WHEN Frequency BETWEEN 0 AND 1 THEN 1
                          WHEN Frequency < 4 THEN 2
                          WHEN Frequency < 7 THEN 3
                          WHEN Frequency >= 7 THEN 4
                    END AS F_SCORE,
                    d.Monetary, Percent_Rank_Monetary,
                    CASE  WHEN Monetary < 120 THEN 1
                          WHEN Monetary BETWEEN 120 AND 500 THEN 2
                          WHEN Monetary BETWEEN 500 AND 1500 THEN 3
                          WHEN Monetary > 1500 THEN 4
                    END AS M_SCORE
                FROM TRANSACTION_DATA a
                LEFT JOIN RECENCY_DATA b
                      ON a.TMNID = b.TMNID
                LEFT JOIN Frequency_DATA c
                      ON a.TMNID = c.TMNID
                LEFT JOIN MONETARY_DATA d
                      ON a.TMNID = d.TMNID )
        SELECT DISTINCT CURRENT_DATE as MODEL_MONTH,'MONTHLY' as RUN_TYPE, a.*,
            CASE  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) = '444' THEN '1_Champion Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('314','324','334','344','414','424','434') THEN '2_Active_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('332', '333', '342', '343', '432', '433', '442', '443') THEN '3_Active_Medium_Value_Sticky_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('312','313','322','323','412','413','422','423') THEN '4_Active_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('311', '321', '331', '341', '411', '421', '431', '441') THEN '5_Active_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('214','224','234','244') THEN '6_Churn_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('212', '213', '222', '223', '232', '233', '242', '243') THEN '7_Churn_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('211','221','231','241') THEN '8_Churn_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('114', '124', '134', '144') THEN '9_Inactive_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('112','113','122','123','132','133','142','143') THEN '10_Inactive_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('121', '131', '141') THEN '11_Inactive_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('111') THEN '12_Inactive_Lowest_Value_Customer'
            END AS Segment
      FROM VIEW a""",'acm-bi.BI_TARGET_LIST.BI_MODEL_VALUE_SEGMENTATION_MASTER')


# ## DIGITAL CONTENT: GOOGLE

# In[15]:


gbq.gbq_insert("""WITH TRANSACTION_DATA AS (SELECT a.TMNID, TRANS_DATETIME, AMOUNT, EXTRACT(month FROM TRANS_DATETIME) AS month,a.SERVICE_LEVEL1,
                                  RANK() OVER (PARTITION BY a.TMNID ORDER BY TRANS_DATETIME DESC ) AS rank   
                            FROM `acm-prod.datamart_std.DM_V1_MOVEMENT_TRANSACTION` a
                            JOIN `acm-prod.datamart.DM_V1_TMNID_MASTER` b
                                  ON a.tmnid = b.TMNID
                                  AND b.DELETED_DATE IS NULL
                            WHERE DATE(TRANS_DATETIME) BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL -180 day) AND CURRENT_DATE
                                  AND a.SERVICE_LEVEL1  = 'DIGITAL CONTENT' and a.SERVICE_LEVEL2  in ('GOOGLE')
                                  AND PMAU_FLAG = 1
                           ),
    RECENCY_DATA AS (SELECT tmnid,
                            DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) AS Recency,
                            PERCENT_RANK() OVER(ORDER BY DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) DESC) AS Percent_Rank_Recency
                        FROM TRANSACTION_DATA
                        WHERE rank = 1),
    FREQUENCY_DATA AS (SELECT tmnid,
                              COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month) AS Frequency,
                              PERCENT_RANK() OVER(ORDER BY COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month)) AS Percent_Rank_Frequency
                         FROM TRANSACTION_DATA
                         GROUP BY 1),
    MONETARY_DATA AS (SELECT tmnid,
                            SUM(AMOUNT)/ COUNT(DISTINCT month) AS Monetary,
                            PERCENT_RANK() OVER(ORDER BY SUM(AMOUNT)/ COUNT(DISTINCT month) ) AS Percent_Rank_Monetary
                          FROM TRANSACTION_DATA 
                          GROUP BY 1 ),
    VIEW AS (SELECT DISTINCT SERVICE_LEVEL1,'GOOGLE' as SERVICE_LEVEL2 , a.tmnid, 
                    b.Recency, Percent_Rank_REcency,
                    CASE  WHEN b.Recency < 8 THEN 4
                          WHEN b.Recency BETWEEN 8 AND 20 THEN 3
                          WHEN b.Recency BETWEEN 20 AND 50 THEN 2
                          WHEN b.Recency >50 THEN 1
                    END AS R_SCORE,
                    c.Frequency, Percent_Rank_Frequency,
                    CASE  WHEN Frequency BETWEEN 0 AND 1 THEN 1
                          WHEN Frequency < 5 THEN 2
                          WHEN Frequency < 8 THEN 3
                          WHEN Frequency >= 8 THEN 4
                    END AS F_SCORE,
                    d.Monetary, Percent_Rank_Monetary,
                    CASE  WHEN Monetary < 70 THEN 1
                          WHEN Monetary BETWEEN 70 AND 250 THEN 2
                          WHEN Monetary BETWEEN 250 AND 600 THEN 3
                          WHEN Monetary > 600 THEN 4
                    END AS M_SCORE
                FROM TRANSACTION_DATA a
                LEFT JOIN RECENCY_DATA b
                      ON a.TMNID = b.TMNID
                LEFT JOIN Frequency_DATA c
                      ON a.TMNID = c.TMNID
                LEFT JOIN MONETARY_DATA d
                      ON a.TMNID = d.TMNID )
        SELECT DISTINCT CURRENT_DATE as MODEL_MONTH,'MONTHLY' as RUN_TYPE, a.*,
            CASE  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) = '444' THEN '1_Champion Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('314','324','334','344','414','424','434') THEN '2_Active_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('332', '333', '342', '343', '432', '433', '442', '443') THEN '3_Active_Medium_Value_Sticky_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('312','313','322','323','412','413','422','423') THEN '4_Active_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('311', '321', '331', '341', '411', '421', '431', '441') THEN '5_Active_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('214','224','234','244') THEN '6_Churn_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('212', '213', '222', '223', '232', '233', '242', '243') THEN '7_Churn_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('211','221','231','241') THEN '8_Churn_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('114', '124', '134', '144') THEN '9_Inactive_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('112','113','122','123','132','133','142','143') THEN '10_Inactive_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('121', '131', '141') THEN '11_Inactive_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('111') THEN '12_Inactive_Lowest_Value_Customer'
            END AS Segment
      FROM VIEW a""",'acm-bi.BI_TARGET_LIST.BI_MODEL_VALUE_SEGMENTATION_MASTER')


# ## DIGITAL CONTENT: APPLE

# In[16]:


gbq.gbq_insert("""WITH TRANSACTION_DATA AS (SELECT a.TMNID, TRANS_DATETIME, AMOUNT, EXTRACT(month FROM TRANS_DATETIME) AS month,a.SERVICE_LEVEL1,
                                  RANK() OVER (PARTITION BY a.TMNID ORDER BY TRANS_DATETIME DESC ) AS rank   
                            FROM `acm-prod.datamart_std.DM_V1_MOVEMENT_TRANSACTION` a
                            JOIN `acm-prod.datamart.DM_V1_TMNID_MASTER` b
                                  ON a.tmnid = b.TMNID
                                  AND b.DELETED_DATE IS NULL
                            WHERE DATE(TRANS_DATETIME) BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL -180 day) AND CURRENT_DATE
                                  AND a.SERVICE_LEVEL1  = 'DIGITAL CONTENT' and a.SERVICE_LEVEL2  in ('APPLE')
                                  AND PMAU_FLAG = 1
                           ),
    RECENCY_DATA AS (SELECT tmnid,
                            DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) AS Recency,
                            PERCENT_RANK() OVER(ORDER BY DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) DESC) AS Percent_Rank_Recency
                        FROM TRANSACTION_DATA
                        WHERE rank = 1),
    FREQUENCY_DATA AS (SELECT tmnid,
                              COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month) AS Frequency,
                              PERCENT_RANK() OVER(ORDER BY COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month)) AS Percent_Rank_Frequency
                         FROM TRANSACTION_DATA
                         GROUP BY 1),
    MONETARY_DATA AS (SELECT tmnid,
                            SUM(AMOUNT)/ COUNT(DISTINCT month) AS Monetary,
                            PERCENT_RANK() OVER(ORDER BY SUM(AMOUNT)/ COUNT(DISTINCT month) ) AS Percent_Rank_Monetary
                          FROM TRANSACTION_DATA 
                          GROUP BY 1 ),
    VIEW AS (SELECT DISTINCT SERVICE_LEVEL1,'APPLE' as SERVICE_LEVEL2 , a.tmnid, 
                    b.Recency, Percent_Rank_REcency,
                    CASE  WHEN b.Recency < 8 THEN 4
                          WHEN b.Recency BETWEEN 8 AND 20 THEN 3
                          WHEN b.Recency BETWEEN 20 AND 50 THEN 2
                          WHEN b.Recency >50 THEN 1
                    END AS R_SCORE,
                    c.Frequency, Percent_Rank_Frequency,
                    CASE  WHEN Frequency BETWEEN 0 AND 1 THEN 1
                          WHEN Frequency < 5 THEN 2
                          WHEN Frequency < 8 THEN 3
                          WHEN Frequency >= 8 THEN 4
                    END AS F_SCORE,
                    d.Monetary, Percent_Rank_Monetary,
                    CASE  WHEN Monetary < 100 THEN 1
                          WHEN Monetary BETWEEN 100 AND 300 THEN 2
                          WHEN Monetary BETWEEN 300 AND 900 THEN 3
                          WHEN Monetary > 900 THEN 4
                    END AS M_SCORE
                FROM TRANSACTION_DATA a
                LEFT JOIN RECENCY_DATA b
                      ON a.TMNID = b.TMNID
                LEFT JOIN Frequency_DATA c
                      ON a.TMNID = c.TMNID
                LEFT JOIN MONETARY_DATA d
                      ON a.TMNID = d.TMNID )
        SELECT DISTINCT CURRENT_DATE as MODEL_MONTH,'MONTHLY' as RUN_TYPE, a.*,
            CASE  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) = '444' THEN '1_Champion Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('314','324','334','344','414','424','434') THEN '2_Active_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('332', '333', '342', '343', '432', '433', '442', '443') THEN '3_Active_Medium_Value_Sticky_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('312','313','322','323','412','413','422','423') THEN '4_Active_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('311', '321', '331', '341', '411', '421', '431', '441') THEN '5_Active_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('214','224','234','244') THEN '6_Churn_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('212', '213', '222', '223', '232', '233', '242', '243') THEN '7_Churn_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('211','221','231','241') THEN '8_Churn_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('114', '124', '134', '144') THEN '9_Inactive_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('112','113','122','123','132','133','142','143') THEN '10_Inactive_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('121', '131', '141') THEN '11_Inactive_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('111') THEN '12_Inactive_Lowest_Value_Customer'
            END AS Segment
      FROM VIEW a""",'acm-bi.BI_TARGET_LIST.BI_MODEL_VALUE_SEGMENTATION_MASTER')


# ## DIGITAL CONTENT: GAMER

# In[17]:


gbq.gbq_insert("""WITH TRANSACTION_DATA AS (SELECT a.TMNID, TRANS_DATETIME, AMOUNT, EXTRACT(month FROM TRANS_DATETIME) AS month,a.SERVICE_LEVEL1,
                                  RANK() OVER (PARTITION BY a.TMNID ORDER BY TRANS_DATETIME DESC ) AS rank   
                            FROM `acm-prod.datamart_std.DM_V1_MOVEMENT_TRANSACTION` a
                            JOIN `acm-prod.datamart.DM_V1_TMNID_MASTER` b
                                  ON a.tmnid = b.TMNID
                                  AND b.DELETED_DATE IS NULL
                            WHERE DATE(TRANS_DATETIME) BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL -180 day) AND CURRENT_DATE
                                  AND a.SERVICE_LEVEL1  = 'DIGITAL CONTENT' and a.SERVICE_LEVEL2  in ('E-PIN','STEAM AND VALVE','LOCAL MERCHANT OTHERS','MOL')
                                  AND PMAU_FLAG = 1
                           ),
    RECENCY_DATA AS (SELECT tmnid,
                            DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) AS Recency,
                            PERCENT_RANK() OVER(ORDER BY DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) DESC) AS Percent_Rank_Recency
                        FROM TRANSACTION_DATA
                        WHERE rank = 1),
    FREQUENCY_DATA AS (SELECT tmnid,
                              COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month) AS Frequency,
                              PERCENT_RANK() OVER(ORDER BY COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month)) AS Percent_Rank_Frequency
                         FROM TRANSACTION_DATA
                         GROUP BY 1),
    MONETARY_DATA AS (SELECT tmnid,
                            SUM(AMOUNT)/ COUNT(DISTINCT month) AS Monetary,
                            PERCENT_RANK() OVER(ORDER BY SUM(AMOUNT)/ COUNT(DISTINCT month) ) AS Percent_Rank_Monetary
                          FROM TRANSACTION_DATA 
                          GROUP BY 1 ),
    VIEW AS (SELECT DISTINCT SERVICE_LEVEL1,'GAMER' as SERVICE_LEVEL2 , a.tmnid, 
                    b.Recency, Percent_Rank_REcency,
                    CASE  WHEN b.Recency < 8 THEN 4
                          WHEN b.Recency BETWEEN 8 AND 20 THEN 3
                          WHEN b.Recency BETWEEN 20 AND 50 THEN 2
                          WHEN b.Recency >50 THEN 1
                    END AS R_SCORE,
                    c.Frequency, Percent_Rank_Frequency,
                    CASE  WHEN Frequency BETWEEN 0 AND 1 THEN 1
                          WHEN Frequency < 5 THEN 2
                          WHEN Frequency < 8 THEN 3
                          WHEN Frequency >= 8 THEN 4
                    END AS F_SCORE,
                    d.Monetary, Percent_Rank_Monetary,
                    CASE  WHEN Monetary < 90 THEN 1
                          WHEN Monetary BETWEEN 90 AND 300 THEN 2
                          WHEN Monetary BETWEEN 300 AND 800 THEN 3
                          WHEN Monetary > 800 THEN 4
                    END AS M_SCORE
                FROM TRANSACTION_DATA a
                LEFT JOIN RECENCY_DATA b
                      ON a.TMNID = b.TMNID
                LEFT JOIN Frequency_DATA c
                      ON a.TMNID = c.TMNID
                LEFT JOIN MONETARY_DATA d
                      ON a.TMNID = d.TMNID )
        SELECT DISTINCT CURRENT_DATE as MODEL_MONTH,'MONTHLY' as RUN_TYPE, a.*,
            CASE  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) = '444' THEN '1_Champion Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('314','324','334','344','414','424','434') THEN '2_Active_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('332', '333', '342', '343', '432', '433', '442', '443') THEN '3_Active_Medium_Value_Sticky_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('312','313','322','323','412','413','422','423') THEN '4_Active_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('311', '321', '331', '341', '411', '421', '431', '441') THEN '5_Active_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('214','224','234','244') THEN '6_Churn_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('212', '213', '222', '223', '232', '233', '242', '243') THEN '7_Churn_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('211','221','231','241') THEN '8_Churn_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('114', '124', '134', '144') THEN '9_Inactive_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('112','113','122','123','132','133','142','143') THEN '10_Inactive_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('121', '131', '141') THEN '11_Inactive_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('111') THEN '12_Inactive_Lowest_Value_Customer'
            END AS Segment
      FROM VIEW a""",'acm-bi.BI_TARGET_LIST.BI_MODEL_VALUE_SEGMENTATION_MASTER')


# ## RETAIL PAYMENT: UNORGANIZED

# In[18]:


gbq.gbq_insert("""WITH TRANSACTION_DATA AS (SELECT a.TMNID, TRANS_DATETIME, AMOUNT, EXTRACT(month FROM TRANS_DATETIME) AS month,a.SERVICE_LEVEL1,
                                  RANK() OVER (PARTITION BY a.TMNID ORDER BY TRANS_DATETIME DESC ) AS rank   
                            FROM `acm-prod.datamart_std.DM_V1_MOVEMENT_TRANSACTION` a
                            JOIN `acm-prod.datamart.DM_V1_TMNID_MASTER` b
                                  ON a.tmnid = b.TMNID
                                  AND b.DELETED_DATE IS NULL
                            WHERE DATE(TRANS_DATETIME) BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL -180 day) AND CURRENT_DATE
                                  AND a.SERVICE_LEVEL1  = 'RETAIL PAYMENT' and a.SERVICE_LEVEL2  = 'UNORGANIZED'
                                  AND PMAU_FLAG = 1
                           ),
    RECENCY_DATA AS (SELECT tmnid,
                            DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) AS Recency,
                            PERCENT_RANK() OVER(ORDER BY DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) DESC) AS Percent_Rank_Recency
                        FROM TRANSACTION_DATA
                        WHERE rank = 1),
    FREQUENCY_DATA AS (SELECT tmnid,
                              COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month) AS Frequency,
                              PERCENT_RANK() OVER(ORDER BY COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month)) AS Percent_Rank_Frequency
                         FROM TRANSACTION_DATA
                         GROUP BY 1),
    MONETARY_DATA AS (SELECT tmnid,
                            SUM(AMOUNT)/ COUNT(DISTINCT month) AS Monetary,
                            PERCENT_RANK() OVER(ORDER BY SUM(AMOUNT)/ COUNT(DISTINCT month) ) AS Percent_Rank_Monetary
                          FROM TRANSACTION_DATA 
                          GROUP BY 1 ),
    VIEW AS (SELECT DISTINCT SERVICE_LEVEL1,'UNORGANIZED'  as SERVICE_LEVEL2 , a.tmnid, 
                    b.Recency, Percent_Rank_REcency,
                    CASE  WHEN b.Recency < 8 THEN 4
                          WHEN b.Recency BETWEEN 8 AND 20 THEN 3
                          WHEN b.Recency BETWEEN 20 AND 50 THEN 2
                          WHEN b.Recency >50 THEN 1
                    END AS R_SCORE,
                    c.Frequency, Percent_Rank_Frequency,
                    CASE  WHEN Frequency BETWEEN 0 AND 1 THEN 1
                          WHEN Frequency < 5 THEN 2
                          WHEN Frequency < 12 THEN 3
                          WHEN Frequency >= 12 THEN 4
                    END AS F_SCORE,
                    d.Monetary, Percent_Rank_Monetary,
                    CASE  WHEN Monetary < 100 THEN 1
                          WHEN Monetary BETWEEN 100 AND 400 THEN 2
                          WHEN Monetary BETWEEN 400 AND 700 THEN 3
                          WHEN Monetary > 700 THEN 4
                    END AS M_SCORE
                FROM TRANSACTION_DATA a
                LEFT JOIN RECENCY_DATA b
                      ON a.TMNID = b.TMNID
                LEFT JOIN Frequency_DATA c
                      ON a.TMNID = c.TMNID
                LEFT JOIN MONETARY_DATA d
                      ON a.TMNID = d.TMNID )
        SELECT DISTINCT CURRENT_DATE as MODEL_MONTH,'MONTHLY' as RUN_TYPE, a.*,
            CASE  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) = '444' THEN '1_Champion Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('314','324','334','344','414','424','434') THEN '2_Active_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('332', '333', '342', '343', '432', '433', '442', '443') THEN '3_Active_Medium_Value_Sticky_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('312','313','322','323','412','413','422','423') THEN '4_Active_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('311', '321', '331', '341', '411', '421', '431', '441') THEN '5_Active_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('214','224','234','244') THEN '6_Churn_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('212', '213', '222', '223', '232', '233', '242', '243') THEN '7_Churn_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('211','221','231','241') THEN '8_Churn_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('114', '124', '134', '144') THEN '9_Inactive_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('112','113','122','123','132','133','142','143') THEN '10_Inactive_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('121', '131', '141') THEN '11_Inactive_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('111') THEN '12_Inactive_Lowest_Value_Customer'
            END AS Segment, 
      FROM VIEW a""",'acm-bi.BI_TARGET_LIST.BI_MODEL_VALUE_SEGMENTATION_MASTER')


# ## RETAIL PAYMENT: MAKRO

# In[19]:


gbq.gbq_insert("""WITH UNION_DATA AS (SELECT a.TMNID, TRANS_DATETIME, AMOUNT, EXTRACT(month FROM TRANS_DATETIME) AS month,a.SERVICE_LEVEL1
                            FROM `acm-prod.datamart_std.DM_V1_MOVEMENT_TRANSACTION` a
                            JOIN `acm-prod.datamart.DM_V1_TMNID_MASTER` b
                                  ON a.tmnid = b.TMNID
                                  AND b.DELETED_DATE IS NULL
                            WHERE DATE(TRANS_DATETIME) BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL -180 day) AND CURRENT_DATE
                                  AND DATE(TRANS_DATETIME) >= '2021-01-01'
                                  AND a.SERVICE_LEVEL1  = 'RETAIL PAYMENT' and a.SERVICE_LEVEL2  in ('MAKRO')
                                  AND PMAU_FLAG = 1
UNION ALL
SELECT a.TMNID, TRANS_DATETIME, AMOUNT, EXTRACT(month FROM TRANS_DATETIME) AS month,a.SERVICE_LEVEL1
                            FROM `acm-prod.datamart_bi.DM_V1_MOVEMENT_TRANSACTION_2020` a
                            JOIN `acm-prod.datamart.DM_V1_TMNID_MASTER` b
                                  ON a.tmnid = b.TMNID
                                  AND b.DELETED_DATE IS NULL
                            WHERE DATE(TRANS_DATETIME) BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL -180 day) AND CURRENT_DATE
                                  AND DATE(TRANS_DATETIME) >= '2020-01-01'
                                  AND a.SERVICE_LEVEL1  = 'RETAIL PAYMENT' and a.SERVICE_LEVEL2  in ('MAKRO')
                                  AND PMAU_FLAG = 1
                                  
                                  ),
                                  
    TRANSACTION_DATA AS  (select *,RANK() OVER (PARTITION BY TMNID ORDER BY TRANS_DATETIME DESC ) AS rank
                                  from UNION_DATA
                               ),
    RECENCY_DATA AS (SELECT tmnid,
                            DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) AS Recency,
                            PERCENT_RANK() OVER(ORDER BY DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) DESC) AS Percent_Rank_Recency
                        FROM TRANSACTION_DATA
                        WHERE rank = 1),
    FREQUENCY_DATA AS (SELECT tmnid,
                              COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month) AS Frequency,
                              PERCENT_RANK() OVER(ORDER BY COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month)) AS Percent_Rank_Frequency
                         FROM TRANSACTION_DATA
                         GROUP BY 1),
    MONETARY_DATA AS (SELECT tmnid,
                            SUM(AMOUNT)/ COUNT(DISTINCT month) AS Monetary,
                            PERCENT_RANK() OVER(ORDER BY SUM(AMOUNT)/ COUNT(DISTINCT month) ) AS Percent_Rank_Monetary
                          FROM TRANSACTION_DATA 
                          GROUP BY 1 ),
    VIEW AS (SELECT DISTINCT SERVICE_LEVEL1,'MAKRO' as SERVICE_LEVEL2 , a.tmnid, 
                    b.Recency, Percent_Rank_REcency,
                    CASE  WHEN b.Recency < 8 THEN 4
                          WHEN b.Recency BETWEEN 8 AND 20 THEN 3
                          WHEN b.Recency BETWEEN 20 AND 50 THEN 2
                          WHEN b.Recency >50 THEN 1
                    END AS R_SCORE,
                    c.Frequency, Percent_Rank_Frequency,
                    CASE  WHEN Frequency BETWEEN 0 AND 1 THEN 1
                          WHEN Frequency < 4 THEN 2
                          WHEN Frequency < 8 THEN 3
                          WHEN Frequency >= 8 THEN 4
                    END AS F_SCORE,
                    d.Monetary, Percent_Rank_Monetary,
                    CASE  WHEN Monetary < 900 THEN 1
                          WHEN Monetary BETWEEN 900 AND 3000 THEN 2
                          WHEN Monetary BETWEEN 3000 AND 7500 THEN 3
                          WHEN Monetary > 7500 THEN 4
                    END AS M_SCORE
                FROM TRANSACTION_DATA a
                LEFT JOIN RECENCY_DATA b
                      ON a.TMNID = b.TMNID
                LEFT JOIN Frequency_DATA c
                      ON a.TMNID = c.TMNID
                LEFT JOIN MONETARY_DATA d
                      ON a.TMNID = d.TMNID )
        SELECT DISTINCT CURRENT_DATE as MODEL_MONTH,'MONTHLY' as RUN_TYPE, a.*,
            CASE  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) = '444' THEN '1_Champion Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('314','324','334','344','414','424','434') THEN '2_Active_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('332', '333', '342', '343', '432', '433', '442', '443') THEN '3_Active_Medium_Value_Sticky_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('312','313','322','323','412','413','422','423') THEN '4_Active_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('311', '321', '331', '341', '411', '421', '431', '441') THEN '5_Active_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('214','224','234','244') THEN '6_Churn_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('212', '213', '222', '223', '232', '233', '242', '243') THEN '7_Churn_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('211','221','231','241') THEN '8_Churn_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('114', '124', '134', '144') THEN '9_Inactive_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('112','113','122','123','132','133','142','143') THEN '10_Inactive_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('121', '131', '141') THEN '11_Inactive_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('111') THEN '12_Inactive_Lowest_Value_Customer'
            END AS Segment
      FROM VIEW a""",'acm-bi.BI_TARGET_LIST.BI_MODEL_VALUE_SEGMENTATION_MASTER')


# ## RETAIL PAYMENT: ORGANIZED

# In[20]:


gbq.gbq_insert("""WITH UNION_DATA AS (SELECT a.TMNID, TRANS_DATETIME, AMOUNT, EXTRACT(month FROM TRANS_DATETIME) AS month,a.SERVICE_LEVEL1
                            FROM `acm-prod.datamart_std.DM_V1_MOVEMENT_TRANSACTION` a
                            JOIN `acm-prod.datamart.DM_V1_TMNID_MASTER` b
                                  ON a.tmnid = b.TMNID
                                  AND b.DELETED_DATE IS NULL
                            WHERE DATE(TRANS_DATETIME) BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL -180 day) AND CURRENT_DATE
                                  AND DATE(TRANS_DATETIME) >= '2021-01-01'
                                  AND a.SERVICE_LEVEL1  = 'RETAIL PAYMENT' and a.SERVICE_LEVEL2  in ('ORGANIZED')
                                  AND PMAU_FLAG = 1
UNION ALL
SELECT a.TMNID, TRANS_DATETIME, AMOUNT, EXTRACT(month FROM TRANS_DATETIME) AS month,a.SERVICE_LEVEL1
                            FROM `acm-prod.datamart_bi.DM_V1_MOVEMENT_TRANSACTION_2020` a
                            JOIN `acm-prod.datamart.DM_V1_TMNID_MASTER` b
                                  ON a.tmnid = b.TMNID
                                  AND b.DELETED_DATE IS NULL
                            WHERE DATE(TRANS_DATETIME) BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL -180 day) AND CURRENT_DATE
                                  AND DATE(TRANS_DATETIME) >= '2020-01-01'
                                  AND a.SERVICE_LEVEL1  = 'RETAIL PAYMENT' and a.SERVICE_LEVEL2  in ('ORGANIZED')
                                  AND PMAU_FLAG = 1
                                  
                                  ),
                                  
    TRANSACTION_DATA AS  (select *,RANK() OVER (PARTITION BY TMNID ORDER BY TRANS_DATETIME DESC ) AS rank
                                  from UNION_DATA
                               ),
    RECENCY_DATA AS (SELECT tmnid,
                            DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) AS Recency,
                            PERCENT_RANK() OVER(ORDER BY DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) DESC) AS Percent_Rank_Recency
                        FROM TRANSACTION_DATA
                        WHERE rank = 1),
    FREQUENCY_DATA AS (SELECT tmnid,
                              COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month) AS Frequency,
                              PERCENT_RANK() OVER(ORDER BY COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month)) AS Percent_Rank_Frequency
                         FROM TRANSACTION_DATA
                         GROUP BY 1),
    MONETARY_DATA AS (SELECT tmnid,
                            SUM(AMOUNT)/ COUNT(DISTINCT month) AS Monetary,
                            PERCENT_RANK() OVER(ORDER BY SUM(AMOUNT)/ COUNT(DISTINCT month) ) AS Percent_Rank_Monetary
                          FROM TRANSACTION_DATA 
                          GROUP BY 1 ),
    VIEW AS (SELECT DISTINCT SERVICE_LEVEL1,'ORGANIZED' as SERVICE_LEVEL2 , a.tmnid, 
                    b.Recency, Percent_Rank_REcency,
                    CASE  WHEN b.Recency < 8 THEN 4
                          WHEN b.Recency BETWEEN 8 AND 20 THEN 3
                          WHEN b.Recency BETWEEN 20 AND 50 THEN 2
                          WHEN b.Recency >50 THEN 1
                    END AS R_SCORE,
                    c.Frequency, Percent_Rank_Frequency,
                    CASE  WHEN Frequency BETWEEN 0 AND 1 THEN 1
                          WHEN Frequency < 4 THEN 2
                          WHEN Frequency < 8 THEN 3
                          WHEN Frequency >= 8 THEN 4
                    END AS F_SCORE,
                    d.Monetary, Percent_Rank_Monetary,
                    CASE  WHEN Monetary < 80 THEN 1
                          WHEN Monetary BETWEEN 80 AND 300 THEN 2
                          WHEN Monetary BETWEEN 300 AND 800 THEN 3
                          WHEN Monetary > 800 THEN 4
                    END AS M_SCORE
                FROM TRANSACTION_DATA a
                LEFT JOIN RECENCY_DATA b
                      ON a.TMNID = b.TMNID
                LEFT JOIN Frequency_DATA c
                      ON a.TMNID = c.TMNID
                LEFT JOIN MONETARY_DATA d
                      ON a.TMNID = d.TMNID )
        SELECT DISTINCT CURRENT_DATE as MODEL_MONTH,'MONTHLY' as RUN_TYPE, a.*,
            CASE  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) = '444' THEN '1_Champion Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('314','324','334','344','414','424','434') THEN '2_Active_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('332', '333', '342', '343', '432', '433', '442', '443') THEN '3_Active_Medium_Value_Sticky_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('312','313','322','323','412','413','422','423') THEN '4_Active_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('311', '321', '331', '341', '411', '421', '431', '441') THEN '5_Active_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('214','224','234','244') THEN '6_Churn_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('212', '213', '222', '223', '232', '233', '242', '243') THEN '7_Churn_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('211','221','231','241') THEN '8_Churn_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('114', '124', '134', '144') THEN '9_Inactive_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('112','113','122','123','132','133','142','143') THEN '10_Inactive_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('121', '131', '141') THEN '11_Inactive_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('111') THEN '12_Inactive_Lowest_Value_Customer'
            END AS Segment
      FROM VIEW a""",'acm-bi.BI_TARGET_LIST.BI_MODEL_VALUE_SEGMENTATION_MASTER')


# ## FINANCIAL PRODUCT: SAVING

# In[21]:


gbq.gbq_insert("""WITH UNION_DATA AS (SELECT a.TMNID, TRANS_DATETIME, AMOUNT, EXTRACT(month FROM TRANS_DATETIME) AS month,a.SERVICE_LEVEL1
                            FROM `acm-prod.datamart_std.DM_V1_MOVEMENT_TRANSACTION` a
                            JOIN `acm-prod.datamart.DM_V1_TMNID_MASTER` b
                                  ON a.tmnid = b.TMNID
                                  AND b.DELETED_DATE IS NULL
                            WHERE DATE(TRANS_DATETIME) BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL -180 day) AND CURRENT_DATE
                                  AND DATE(TRANS_DATETIME) >= '2021-01-01'
                                  AND a.SERVICE_LEVEL1  = 'FINANCIAL PRODUCT' and a.SERVICE_LEVEL2  in ('SAVING')
                                  AND PMAU_FLAG = 1
UNION ALL
SELECT a.TMNID, TRANS_DATETIME, AMOUNT, EXTRACT(month FROM TRANS_DATETIME) AS month,a.SERVICE_LEVEL1
                            FROM `acm-prod.datamart_bi.DM_V1_MOVEMENT_TRANSACTION_2020` a
                            JOIN `acm-prod.datamart.DM_V1_TMNID_MASTER` b
                                  ON a.tmnid = b.TMNID
                                  AND b.DELETED_DATE IS NULL
                            WHERE DATE(TRANS_DATETIME) BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL -180 day) AND CURRENT_DATE
                                  AND DATE(TRANS_DATETIME) >= '2020-01-01'
                                  AND a.SERVICE_LEVEL1  = 'FINANCIAL PRODUCT' and a.SERVICE_LEVEL2  in ('SAVING')
                                  AND PMAU_FLAG = 1
                                  
                                  ),
                                  
    TRANSACTION_DATA AS  (select *,RANK() OVER (PARTITION BY TMNID ORDER BY TRANS_DATETIME DESC ) AS rank
                                  from UNION_DATA
                               ),
    RECENCY_DATA AS (SELECT tmnid,
                            DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) AS Recency,
                            PERCENT_RANK() OVER(ORDER BY DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) DESC) AS Percent_Rank_Recency
                        FROM TRANSACTION_DATA
                        WHERE rank = 1),
    FREQUENCY_DATA AS (SELECT tmnid,
                              COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month) AS Frequency,
                              PERCENT_RANK() OVER(ORDER BY COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month)) AS Percent_Rank_Frequency
                         FROM TRANSACTION_DATA
                         GROUP BY 1),
    MONETARY_DATA AS (SELECT tmnid,
                            SUM(AMOUNT)/ COUNT(DISTINCT month) AS Monetary,
                            PERCENT_RANK() OVER(ORDER BY SUM(AMOUNT)/ COUNT(DISTINCT month) ) AS Percent_Rank_Monetary
                          FROM TRANSACTION_DATA 
                          GROUP BY 1 ),
    VIEW AS (SELECT DISTINCT SERVICE_LEVEL1,'SAVING' as SERVICE_LEVEL2 , a.tmnid, 
                    b.Recency, Percent_Rank_REcency,
                    CASE  WHEN b.Recency < 8 THEN 4
                          WHEN b.Recency BETWEEN 8 AND 20 THEN 3
                          WHEN b.Recency BETWEEN 20 AND 50 THEN 2
                          WHEN b.Recency >50 THEN 1
                    END AS R_SCORE,
                    c.Frequency, Percent_Rank_Frequency,
                    CASE  WHEN Frequency BETWEEN 0 AND 1 THEN 1
                          WHEN Frequency < 6 THEN 2
                          WHEN Frequency < 12 THEN 3
                          WHEN Frequency >= 12 THEN 4
                    END AS F_SCORE,
                    d.Monetary, Percent_Rank_Monetary,
                    CASE  WHEN Monetary < 300 THEN 1
                          WHEN Monetary BETWEEN 300 AND 3500 THEN 2
                          WHEN Monetary BETWEEN 3500 AND 18000 THEN 3
                          WHEN Monetary > 18000 THEN 4
                    END AS M_SCORE
                FROM TRANSACTION_DATA a
                LEFT JOIN RECENCY_DATA b
                      ON a.TMNID = b.TMNID
                LEFT JOIN Frequency_DATA c
                      ON a.TMNID = c.TMNID
                LEFT JOIN MONETARY_DATA d
                      ON a.TMNID = d.TMNID )
        SELECT DISTINCT CURRENT_DATE as MODEL_MONTH,'MONTHLY' as RUN_TYPE, a.*,
            CASE  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) = '444' THEN '1_Champion Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('314','324','334','344','414','424','434') THEN '2_Active_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('332', '333', '342', '343', '432', '433', '442', '443') THEN '3_Active_Medium_Value_Sticky_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('312','313','322','323','412','413','422','423') THEN '4_Active_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('311', '321', '331', '341', '411', '421', '431', '441') THEN '5_Active_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('214','224','234','244') THEN '6_Churn_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('212', '213', '222', '223', '232', '233', '242', '243') THEN '7_Churn_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('211','221','231','241') THEN '8_Churn_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('114', '124', '134', '144') THEN '9_Inactive_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('112','113','122','123','132','133','142','143') THEN '10_Inactive_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('121', '131', '141') THEN '11_Inactive_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('111') THEN '12_Inactive_Lowest_Value_Customer'
            END AS Segment
      FROM VIEW a""",'acm-bi.BI_TARGET_LIST.BI_MODEL_VALUE_SEGMENTATION_MASTER')


# ## TRANSPORTATION: MRT

# In[22]:


gbq.gbq_insert("""WITH UNION_DATA AS (SELECT a.TMNID, TRANS_DATETIME, AMOUNT, EXTRACT(month FROM TRANS_DATETIME) AS month,a.SERVICE_LEVEL1
                            FROM `acm-prod.datamart_std.DM_V1_MOVEMENT_TRANSACTION` a
                            JOIN `acm-prod.datamart.DM_V1_TMNID_MASTER` b
                                  ON a.tmnid = b.TMNID
                                  AND b.DELETED_DATE IS NULL
                            WHERE DATE(TRANS_DATETIME) BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL -180 day) AND CURRENT_DATE
                                  AND DATE(TRANS_DATETIME) >= '2021-01-01'
                                  AND a.SERVICE_LEVEL1  = 'TRANSPORTATION' and a.SERVICE_LEVEL2  in ('MRT')
                                  AND PMAU_FLAG = 1
UNION ALL
SELECT a.TMNID, TRANS_DATETIME, AMOUNT, EXTRACT(month FROM TRANS_DATETIME) AS month,a.SERVICE_LEVEL1
                            FROM `acm-prod.datamart_bi.DM_V1_MOVEMENT_TRANSACTION_2020` a
                            JOIN `acm-prod.datamart.DM_V1_TMNID_MASTER` b
                                  ON a.tmnid = b.TMNID
                                  AND b.DELETED_DATE IS NULL
                            WHERE DATE(TRANS_DATETIME) BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL -180 day) AND CURRENT_DATE
                                  AND DATE(TRANS_DATETIME) >= '2020-01-01'
                                  AND a.SERVICE_LEVEL1  = 'TRANSPORTATION' and a.SERVICE_LEVEL2  in ('MRT')
                                  AND PMAU_FLAG = 1
                                  
                                  ),
                                  
    TRANSACTION_DATA AS  (select *,RANK() OVER (PARTITION BY TMNID ORDER BY TRANS_DATETIME DESC ) AS rank
                                  from UNION_DATA
                               ),
    RECENCY_DATA AS (SELECT tmnid,
                            DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) AS Recency,
                            PERCENT_RANK() OVER(ORDER BY DATE_DIFF(CURRENT_DATE, DATE(TRANS_DATETIME), day) DESC) AS Percent_Rank_Recency
                        FROM TRANSACTION_DATA
                        WHERE rank = 1),
    FREQUENCY_DATA AS (SELECT tmnid,
                              COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month) AS Frequency,
                              PERCENT_RANK() OVER(ORDER BY COUNT(DISTINCT(DATE(TRANS_DATETIME)))/ COUNT(DISTINCT month)) AS Percent_Rank_Frequency
                         FROM TRANSACTION_DATA
                         GROUP BY 1),
    MONETARY_DATA AS (SELECT tmnid,
                            SUM(AMOUNT)/ COUNT(DISTINCT month) AS Monetary,
                            PERCENT_RANK() OVER(ORDER BY SUM(AMOUNT)/ COUNT(DISTINCT month) ) AS Percent_Rank_Monetary
                          FROM TRANSACTION_DATA 
                          GROUP BY 1 ),
    VIEW AS (SELECT DISTINCT SERVICE_LEVEL1,'MRT' as SERVICE_LEVEL2 , a.tmnid, 
                    b.Recency, Percent_Rank_REcency,
                    CASE  WHEN b.Recency < 8 THEN 4
                          WHEN b.Recency BETWEEN 8 AND 20 THEN 3
                          WHEN b.Recency BETWEEN 20 AND 50 THEN 2
                          WHEN b.Recency >50 THEN 1
                    END AS R_SCORE,
                    c.Frequency, Percent_Rank_Frequency,
                    CASE  WHEN Frequency BETWEEN 0 AND 1 THEN 1
                          WHEN Frequency < 4 THEN 2
                          WHEN Frequency < 6 THEN 3
                          WHEN Frequency >= 6 THEN 4
                    END AS F_SCORE,
                    d.Monetary, Percent_Rank_Monetary,
                    CASE  WHEN Monetary < 200 THEN 1
                          WHEN Monetary BETWEEN 200 AND 400 THEN 2
                          WHEN Monetary BETWEEN 400 AND 800 THEN 3
                          WHEN Monetary > 800 THEN 4
                    END AS M_SCORE
                FROM TRANSACTION_DATA a
                LEFT JOIN RECENCY_DATA b
                      ON a.TMNID = b.TMNID
                LEFT JOIN Frequency_DATA c
                      ON a.TMNID = c.TMNID
                LEFT JOIN MONETARY_DATA d
                      ON a.TMNID = d.TMNID )
        SELECT DISTINCT CURRENT_DATE as MODEL_MONTH,'MONTHLY' as RUN_TYPE, a.*,
            CASE  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) = '444' THEN '1_Champion Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('314','324','334','344','414','424','434') THEN '2_Active_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('332', '333', '342', '343', '432', '433', '442', '443') THEN '3_Active_Medium_Value_Sticky_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('312','313','322','323','412','413','422','423') THEN '4_Active_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('311', '321', '331', '341', '411', '421', '431', '441') THEN '5_Active_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('214','224','234','244') THEN '6_Churn_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('212', '213', '222', '223', '232', '233', '242', '243') THEN '7_Churn_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('211','221','231','241') THEN '8_Churn_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('114', '124', '134', '144') THEN '9_Inactive_High_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('112','113','122','123','132','133','142','143') THEN '10_Inactive_Medium_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('121', '131', '141') THEN '11_Inactive_Low_Value_Customer'
                  WHEN CONCAT(R_SCORE,F_SCORE,M_SCORE) IN ('111') THEN '12_Inactive_Lowest_Value_Customer'
            END AS Segment
      FROM VIEW a""",'acm-bi.BI_TARGET_LIST.BI_MODEL_VALUE_SEGMENTATION_MASTER')


# In[ ]:




